<?php

namespace common\interfaces;
/**
 * Created by PhpStorm.
 * User: Vardan
 * Date: 11/16/2015
 * Time: 1:05 PM
 */
interface MultiLanguageModel
{

    public function addParentId($id);
    public function addPostWhithNewLang($id,$lang);

}