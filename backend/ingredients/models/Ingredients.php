<?php

namespace backend\ingredients\models;

use Yii;
use common\models\Func;
use yii\db\Connection;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\behaviors\Upload;
use yii\db\Expression;
use backend\models\Lang;

use mongosoft\file\UploadImageBehavior;

/**
 * This is the model class for table "Ingredients".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $img
 * @property string $date
 * @property integer $cat_id
 * @property string $long
 * @property integer $parrent_id
 */
class Ingredients extends ActiveRecord
{
    public static  $update = null;
    public $images;

    public function rules()
    {
        return [
//            [['lang'], 'required'],
            [['id', 'cat_id', 'parrent_id','product_id'], 'integer'],
            [['description','slug'], 'string'],
            [['title'], 'string', 'max' => 250],
            [['product_id_1', 'product_id_2', 'product_id_3','product_id_4', 'product_id_5'], 'string', 'max' => 30],
            [['created_at'], 'string', 'max' => 50],
            [['updated_at'], 'string', 'max' => 50],
            [['lang'], 'string', 'max' => 10],
            [['video'], 'string', 'max' => 150],
            [['country_of_origin','usage_area','taste_specification','available_weight'], 'string'],
            [['img'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => ['insert', 'update']],
            [['images'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => ['insert', 'update']],
        ];
    }

    /*
   * @slug
   */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'slug',
                'translit' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => Upload::className(),
                'fileName' => 'img',
                'filePath' => 'Ingredients/',
                'width' => 270,
                'height' => 270,
                'update' => self::$update
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ingredients';
    }

    public function addParentId($id){
        $model = $this->findOne($id);

//        Func::d($model);

        $model->parrent_id = $id;
        $model->save();
    }

    public function addPostWhithNewLang($id,$lang){

        $info = $this->find()->where(['id' => $id])->one();

        $connection = \Yii::$app->db;
        $connection->createCommand()->insert('Ingredients', [
            'parrent_id' => $id,
            'lang' => $lang,
            'img' => $info->img,
            'video' => $info->video
        ])->execute();

        $id = Yii::$app->db->getLastInsertID();

        return $this->find()->where(['id' => $id,'lang'=> $lang])->one();
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => '',
            'img' => 'Img',
            'created_at' => 'Date',
            'cat_id' => 'Category',
            'lang' => 'Lang',
            'parrent_id' => 'Parrent ID',
            'slug' => 'Slug'
        ];
    }

    public function findModelCat($ids)
    {
        $recipes = $this->find()->where(['lang' => Lang::getCurrent()->url, 'parrent_id' => $ids]);
        return $recipes;
    }

    public function ingredients($lang)
    {
        $ingredients = $this->find()->where(['lang' => $lang])->select('title,img,parrent_id')->all();
        return $ingredients;
    }
    public function findModelLimit($lang,$limit)
    {
        return $this->find()->where(['lang'=>$lang])->limit($limit)->all();
    }
    public static function findRecentModel($lang,$limit)
    {
        return self::find()->where(['lang' => $lang])->limit($limit)->orderBy('id desc')->all();
    }

    public function recipeIngredient($id,$lang)
    {
        return $this->find()->select('slug, title')->where(['parrent_id' => $id, 'lang' => $lang])->one();
    }
}
