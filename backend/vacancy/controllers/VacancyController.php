<?php

namespace backend\vacancy\controllers;

use Yii;
use backend\vacancy\models\Vacancy;
use backend\vacancy\models\Categories;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Lang;
use common\models\Func;


class VacancyController extends Controller
{
    private $model;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public  function init(){
        $this->model  = new Vacancy();
    }


    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->model->find()->where(['lang' => 'am'])->groupBy('parrent_id'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionCreate()
    {
        $model = $this->model;
        $model->update = null;

        if ($model->load(Yii::$app->request->post())) {
//            $model->cat_id = $_POST['category'];
            $model->lang = 'am';
            $model->save();

            $id = Yii::$app->db->getLastInsertID();
            $model->addParentId($id);

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Posts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$lang = 'am')
    {
        $model = $this->findForUpdate($id,$lang);
        $model->update = true;
        if ($model->load(Yii::$app->request->post())) {

            $model->save();
            return $this->redirect(['update', 'id' => $model->parrent_id,'lang'=>$lang]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        foreach($model as $one){
            $one->delete();
        }
        return $this->redirect(['index']);
    }




    protected function findModel($id)
    {
        if (($model = $this->model->find()->where(['parrent_id' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findForUpdate($id,$lang){


        if(($model = $this->model->find()->where(['parrent_id' => $id,"lang" => $lang])->one()) != null){
            return $model;
        }elseif(($model = $this->model->findOne(['parrent_id' => $id,"lang != :lang", [':lang' => $lang]])) !==null){ 

            $model = $model->addPostWhithNewLang($id,$lang);
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteimg($id){

        unlink(Yii::getAlias('@backend/web/upload/posts/'.$_POST['item'].'/'.$id));
        unlink(Yii::getAlias('@backend/web/upload/posts/'.$_POST['item'].'/thumbs/'.$id));
        echo json_encode($_POST);
    }


}
