<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\about\models\@modelName */

$this->title = 'Create item';
$this->params['breadcrumbs'][] = ['label' => '@item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="posts-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
