<?php

namespace backend\recipe\controllers;

use common\models\Func;
use Yii;
use backend\ingredients\models\Ingredients;
use backend\recipe\models\IngredientsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * IngredientsController implements the CRUD actions for Ingredients model.
 */
class IngredientsController extends Controller
{

    private $model;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public  function init(){
        $this->model  = new Ingredients();
    }

    /**
     * Lists all Ingredients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->model->find()->where(['lang' => 'am'])->groupBy('parrent_id'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ingredients model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Ingredients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ingredients();

        if ($model->load(Yii::$app->request->post())) {

            $model->lang = 'am';
            $model->save();

            $id = Yii::$app->db->getLastInsertID();
            $model->addParentId($id);

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Ingredients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $lang = 'am')
    {
        $model = $this->findForUpdate($id,$lang);
        $model->update = true;

        $img = $model->img;
//        Func::d($model->product_id);

        if ($model->load(Yii::$app->request->post())) {

            $model->img = empty($model->img) ? $img :  $model->img;

            $model->save();

            return $this->redirect(['update', 'id' => $model->parrent_id,'lang'=>$lang]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ingredients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {       $model = $this->findModel($id);

        foreach($model as $one){
            $one->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ingredients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ingredients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = $this->model->find()->where(['parrent_id' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findForUpdate($id,$lang){

        if(($model = $this->model->find()->where(['parrent_id' => $id,"lang" => $lang])->one()) != null){
//            Func::d($model);
            return $model;
        }elseif(($model = $this->model->findOne(['parrent_id' => $id,"lang != :lang", [':lang' => $lang]])) !==null){
            $model = $model->addPostWhithNewLang($id,$lang);
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteimg($id){

        unlink(Yii::getAlias('@backend/web/upload/ingredients/'.$_POST['item'].'/'.$id));
        unlink(Yii::getAlias('@backend/web/upload/ingredients/'.$_POST['item'].'/thumbs/'.$id));
        echo json_encode($_POST);
    }
}
