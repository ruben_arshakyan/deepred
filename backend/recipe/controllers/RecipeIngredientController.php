<?php
/**
 * Created by PhpStorm.
 * User: DRM
 * Date: 28.01.2016
 * Time: 10:42
 */

namespace backend\recipe\controllers;

use backend\recipe\models\Recipe;
use common\models\Func;
use Yii;
use yii\web\Controller;
use backend\recipe\models\RecipeIngredients;
use backend\ingredients\models\Ingredients;

class RecipeIngredientController extends Controller
{

    public function actionEditIngredient($id,$lang = 'am')
    {

        $model = new RecipeIngredients();
        $ingredient = $model->getProduct($id);
        $ing = new Ingredients();
        $productName = $ing->recipeIngredient($ingredient->ingredient_id,$lang);

        if(!empty($_POST)){

            $ingredient->count = $_POST['count'];
            $model->edit($_POST['id'],$ingredient->count);

            return $this->redirect(Yii::$app->request->referrer);
        }else{
            return $this->renderAjax('edit-ingredient',[
                'ingredient' => $ingredient,
                'productName' => $productName
            ]);
        }
    }

    public function actionOtherEditIngredient($id,$lang = 'am')
    {

        $model = new RecipeIngredients();
        $ingredient = $model->findOne($id);
        //Func::d($ingredient);

        if($ingredient->load(Yii::$app->request->post())){

            $model->otherEdit($id, $_POST['RecipeIngredients']);

            return $this->redirect(Yii::$app->request->referrer);
        }else{
            return $this->renderAjax('other-edit-ingredient',[
                'ingredient' => $ingredient,
            ]);
        }
    }


    public function actionDelete($id){

        $rel = new RecipeIngredients();

        $rel->find()->where(['id' => $id])->one()->delete();

        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionUpdate($id)
    {
        Func::d($id);
    }

}