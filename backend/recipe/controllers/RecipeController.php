<?php

namespace backend\recipe\controllers;

use backend\ingredients\models\Ingredients;
use backend\ingredients\models\IngredientsRel;
use backend\recipe\models\RecipeIngredients;
use backend\recipe\models\RecipeRel;
use Yii;
use backend\recipe\models\Recipe;
use backend\recipe\models\Categories;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Lang;
use common\models\Func;
use yii\web\Session;


class RecipeController extends Controller
{
    private $model;
    private $session;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public  function init(){
        $this->model  = new Recipe();
    }


    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->model->find()->where(['lang' => 'am'])->groupBy('parrent_id'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionCreate($lang = 'am')
    {
        $model = $this->model;
        $model->update = null;
        $catRel = new RecipeRel();
//        $ingredientsRel = new RecipeIngredients();

        if ($model->load(Yii::$app->request->post())) {

//            $model->cat_id = $_POST['category'][0];
            $model->lang = 'am';
            $model->save();

            $id = Yii::$app->db->getLastInsertID();
            $model->addParentId($id);

//            foreach ($_POST['category'] as $one) {
//                $catRel->addNew($one, $id);
//            }

//            if($_POST['ing']!=''){
//                foreach ($_SESSION['ing'][$_POST['ing']] as $item) {
//                    $ingredientsRel->addNew($item['id'],$item['count'],$id);
//                }
//            }

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'lang' => $lang
            ]);
        }
    }

    /**
     * Updates an existing Posts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$lang = 'am')
    {
        $model = $this->findForUpdate($id,$lang);
        $model->update = true;
        $catRel = new RecipeRel();
        $ingredients = $this->getProducts($id,$lang);

        if ($model->load(Yii::$app->request->post())) {
//            $cats = $_POST['category'];
//            $model->cat_id = $_POST['category'][0];
//            $catRel->updateItems($cats, $id);
            $model->save();
            return $this->redirect(['update', 'id' => $model->parrent_id,'lang'=>$lang]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'lang' => $lang,
                'ingredients' => $ingredients
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        foreach($model as $one){
            $one->delete();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id)
    {
        if (($model = $this->model->find()->where(['parrent_id' => $id])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findForUpdate($id,$lang){

        if(($model = $this->model->find()->where(['parrent_id' => $id,"lang" => $lang])->one()) != null){
//            Func::d($model);
            return $model;
        }elseif(($model = $this->model->findOne(['parrent_id' => $id,"lang != :lang", [':lang' => $lang]])) !==null){
            $model = $model->addPostWhithNewLang($id,$lang);
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIngredients($lang='am',$id=null)
    {
        $model = $this->findForUpdate($id,$lang);
        $model->update = true;
        $model = new Ingredients();
        $ingredients = $model->ingredients($lang);
        $ingredient = new RecipeIngredients();
        if (!empty($_POST)) {
            foreach($_POST['item'] as $item) {
                if (isset($item['id'])) {
                    $ingredient->addNew($item['id'],$item['count'],$id);
                }
            }
            return $this->redirect(['update', 'id' => $id,'lang'=>$lang]);
        } else {
            return $this->renderAjax('ingredients',[
                'ingredients' => $ingredients,
                'parentId' => $id
            ]);
        }
    }

    public function actionEditIngredient($lsng = 'am', $id)
    {
        $model = $this->findForUpdate($id,$lsng);
        $model->update = true;

        $model = new RecipeIngredients();
        $ingredient = $model->getProduct($id);
        $ing = new Ingredients();
        $productName = $ing->recipeIngredient($ingredient->ingredient_id,$lsng);

        return $this->renderAjax('edit-product',[
            'ingredient' => $ingredient,
            'productName' => $productName
        ]);
    }

    public function actionOtherIngredients($lang='am',$id=null)
    {
        $model = new RecipeIngredients();


        if (!empty($_POST)) {

            $model->addNewOther($_POST['title_am'],$_POST['title_en'],$_POST['title_ru'],$_POST['count'],$id);

            return $this->redirect(['update', 'id' => $id,'lang'=>$lang]);

        } else {
            return $this->renderAjax('other-ingredients',[
                'parentId' => $id
            ]);
        }
    }

    public function getProducts($id,$lang)
    {
        $ingredients = new RecipeIngredients();
        $ingredient = $ingredients->getAllCats($id);
        $ing = new Ingredients();
        $result = '<ul class="todo-list ui-sortable ingredients-list" style="list-style-type: none;">';
        $title = 'title_'.$lang;
        foreach ($ingredient as $item) {
            if($item->ingredient_id != null)
            {
                $result .= '<li><a href="/backend/recipe/recipe-ingredient/edit-ingredient/" data-lang="'  . $lang .'" data-id = "' . $item->id .'" class="editMaranik">'.$ing->recipeIngredient($item->ingredient_id,$lang)->title. ' ' . $item->count .
                            '</a><div class="tools">
                                <a href="/backend/recipe/recipe-ingredient/edit-ingredient/" data-lang="'  . $lang .'" data-id = "' . $item->id .'" class="editMaranik">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="/backend/recipe/recipe-ingredient/delete/'.$item->id.'">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </div>
                            </li>';
            }else{
                $result .= '<li><a href="/backend/recipe/recipe-ingredient/other-edit-ingredient/" data-lang="'  . $lang .'" data-id = "' . $item->id .'" class="otherEditMaranik">'.$item->$title. ' ' . $item->count .
                            '</a><div class="tools">
                                <a href="/backend/recipe/recipe-ingredient/other-edit-ingredient/" data-lang="'  . $lang .'" data-id = "' . $item->id .'" class="otherEditMaranik">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="/backend/recipe/recipe-ingredient/delete/'.$item->id.'">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </div>
                            </li>';
            }
        }
        $result .= '</ul>';

        return $result;
    }



    public function actionDeleteimg($id){

        unlink(Yii::getAlias('@backend/web/upload/recipe/'.$_POST['item'].'/'.$id));
        unlink(Yii::getAlias('@backend/web/upload/recipe/'.$_POST['item'].'/thumbs/'.$id));
        echo json_encode($_POST);
    }




}
