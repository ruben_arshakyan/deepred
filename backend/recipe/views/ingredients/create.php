<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\recipe\models\Ingredients */

$this->title = 'Create Ingredients';
$this->params['breadcrumbs'][] = ['label' => 'Ingredients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredients-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
