<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
        'multiple' => true,
    ]
]); ?>
<input type="hidden" name="id" value="<?php // $ingredient->id ?>">
<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
    <thead>
    <tr role="row">
        <td class="strong">Title Arm</td>
        <td class="strong">Title Eng</td>
        <td class="strong">Title Rus</td>
        <td class="strong">Count</td>

    </tr>
    </thead>
    <tbody>
    <tr role="row">
        <td><?= $form->field($ingredient, 'title_am')->textInput()->label(false)?></td>
        <td><?= $form->field($ingredient, 'title_en')->textInput()->label(false)?></td>
        <td><?= $form->field($ingredient, 'title_ru')->textInput()->label(false)?></td>
        <td><?= $form->field($ingredient, 'count')->textInput()->label(false)?></td>
    </tr>
    </tbody>
</table>

<div class="form-group">
    <?= Html::submitButton( 'Update', ['class' => 'btn btn-primary']) ?>

</div>

<?php $form = ActiveForm::end() ?>
