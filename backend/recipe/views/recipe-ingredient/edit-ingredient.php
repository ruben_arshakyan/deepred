<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
        'multiple' => true,
    ]
]); ?>
<input type="hidden" name="id" value="<?= $ingredient->id ?>">
<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
    <thead>
    <tr role="row">
        <td class="strong">Title</td>
        <td class="strong">Count</td>

    </tr>
    </thead>
    <tbody>
        <tr role="row">
            <td><?= $productName->title ?></td>
            <td><input type="text" name="count" value="<?= $ingredient->count ?>"></td>
        </tr>
    </tbody>
</table>

<div class="form-group">
    <?= Html::submitButton( 'Update', ['class' => 'btn btn-primary']) ?>

</div>

<?php $form = ActiveForm::end() ?>
