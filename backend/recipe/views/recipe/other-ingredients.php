<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\ingredients\models\IngredientsTree;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'multiple' => true,
            'id' => 'ingredients'
        ]
    ]); ?>

<table id="other-ingredients" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
    <thead>
    <tr role="row">
        <td class="strong">Title Arm</td>
        <td class="strong">Title Eng</td>
        <td class="strong">Title Rus</td>
        <td class="strong">Count</td>
    </tr>
    </thead>
    <tbody>
        <tr role="row">
            <td><input type="text" name="title_am"></td>
            <td><input type="text" name="title_en"></td>
            <td><input type="text" name="title_ru"></td>
            <td><input type="text" name="count"></td>
        </tr>
    </tbody>
</table>

<div class="form-group">
    <input type="button" class="btn btn-primary" id="ingredientsSubmit" value="Add product">
</div>

<?php $form = ActiveForm::end() ?>
