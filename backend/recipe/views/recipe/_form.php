<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Lang;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Url;
use yii\bootstrap\Modal;

use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\posts\models\Posts */
/* @var $form yii\widgets\ActiveForm */
$langs = Lang::find()->all();
//\common\models\Func::d($ingredients);

?>

<div class="posts-form">
    <?php if(!$model->isNewRecord){?>
        <ul class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
                Language <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <?php foreach($langs as $one){?>
                    <li role="presentation"><?php echo  Html::a($one->name,['update','id'=> $model->parrent_id,'lang' => $one->url]);?></li>
                <?php }?>
            </ul>
        </ul>

    <?php }?>

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'multiple' => true,
        ]
    ]); ?>

    <div class="row">
        <div class="col-lg-6">
            <div class="box box-success col-lg-12">
                <div class="box-body no-padding" style="display: block;">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

<!--            <div class="box box-success col-lg-12">-->
<!--                <div class="box-body no-padding" style="display: block;">-->
<!--                    <label class="control-label">Categories</label>-->
<!--                    <div class="box-body cat-box no-padding " style="display: block;">-->
<!--                        --><?php
//                        $cats = new \backend\recipe\models\CategoryTree($model->parrent_id);
//                        $cats->outTree(0,0);
//                        ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <?php if(!$model->isNewRecord){?>
            <div class="box box-success col-lg-12">
                <div class="box-body " style="display: block; padding: 0px 0px 20px 0px; ">
                    <label class="control-label">Products</label>
                    <div class="box-body cat-box no-padding " style="display: block;">
                        <?php
                            echo $ingredients;
                        ?>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>



        <div class="col-lg-6">
            <div class="box box-success col-lg-12">
                <div class="col-lg-12 img_div">
                    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true]) ?>

                    <?php
                    echo '<label class="control-label">Add Attachments</label>';
                    if($model->isNewRecord){
                        echo FileInput::widget([
                            'model' => $model,
                            'name' => 'img',
                            'attribute' => 'img',
                        ]);
                    }else{
                        echo FileInput::widget([
                            'model' => $model,
                            'attribute' => 'img',
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'initialPreview'=>[
                                    Html::img("@web/upload/Recipe/thumbs/$model->img", ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon']),
                                ],
                            ],
                        ]);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php if(!$model->isNewRecord){?>
        <p>
            <?= Html::button('Maranik products', ['value' => Url::to('recipe/recipe/ingredients'),'data-lang' => $lang, 'data-id' => $model->parrent_id, 'class' => 'btn btn-primary', 'id' => 'maranikButton']) ?>
            <?= Html::button('Other products', ['value' => Url::to('recipe/recipe/other-ingredients'),'data-lang' => $lang, 'data-id' => $model->parrent_id, 'class' => 'btn btn-primary', 'id' => 'otherButton']) ?>
        </p>
    <?php }?>

    <?php

        Modal::begin([
            'header' => '<h4>Maranik products</h4>',
            'id' => 'maranik',
            'size' => 'model-lg',
        ]);

        echo "<div id='maranikContent'></div>";

        Modal::end()

    ?>

    <?php

    Modal::begin([
        'header' => '<h4>Other products</h4>',
        'id' => 'other',
        'size' => 'model-lg',
    ]);

    echo "<div id='otherContent'></div>";

    Modal::end()

    ?>

    <?php

    Modal::begin([
        'header' => '<h4>Edit product</h4>',
        'id' => 'edit-maranik',
        'size' => 'model-lg',
    ]);

    echo "<div id='editContent'></div>";

    Modal::end()

    ?>

    <div class="box box-success">

        <div class="box-body no-padding" style="display: block;">

                <div class="col-lg-12">
                    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
                        'options' => ['rows' => 8],
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ]);?>
                </div>

        </div><!-- /.box-body -->
    </div>


    <div class="row">
        <div class="form-group col-lg-12">
            <?php
            if(!$model->isNewRecord) {
                echo '<label class="control-label">Add Gallery</label>';
                echo FileInput::widget([
                    'model' => $model,
                    'attribute' => 'images[]',
                    'name' => 'images[]',
                    'options' => [
                        //'accept' => 'image/*',
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                    ],
                ]);
            }
            ?>
        </div>


        <?php
        if(!$model->isNewRecord ){

            $gallery = \common\models\Func::getGallery("recipe/".$model->parrent_id.'/thumbs');

            if($gallery){
                //\common\models\Func::d($gallery);
                ?>

                <div class="form-group col-lg-12">
                    <div class="box box-success collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Show gallery</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body row albom" style="display: none;" id="<?=$model->parrent_id?>">
                            <?php foreach($gallery as $one){ ?>
                                <div class="col-lg-2 gallery_item">
                                    <a class="img_link"  href="<?=$one?>"><i class="fa fa-close"></i></a>
                                    <?php  echo "<img class='file-preview-image' src='".Url::to('@web/upload/recipe/'.$model->parrent_id.'/'.$one)."' >"; ?>
                                </div>


                            <?php }?>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            <?php }?>

        <?php }?>
        <div class="form-group col-lg-12">

            <?= $form->field($model, 'video')->textInput(['maxlength' => true]) ?>

        </div>

        <input type="hidden" name="ing" value="" id="ing">

        <div class="form-group col-lg-12">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
