<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\ingredients\models\IngredientsTree;

$tree = new IngredientsTree($parentId);

?>

<?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'multiple' => true,
            'id' => 'ingredients'
        ]
    ]); ?>

<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
    <thead>
    <tr role="row">
        <td class="strong">Title</td>
        <td class="strong">Count</td>

    </tr>
    </thead>
    <tbody>
    <?php foreach($ingredients as $ingredient){?>
        <tr role="row">
            <?php
            $check = $tree->getChecked($ingredient->parrent_id);
            if($check == ""){
            ?>
            <td><?=$ingredient->title?></td>
            <td><input type="text" name="item[<?= $ingredient->parrent_id?>][count]" value="<?=$tree->getCount($ingredient->parrent_id)?>"></td>
            <td><input type="checkbox" name="item[<?= $ingredient->parrent_id?>][id]" value="<?=$ingredient->parrent_id?>" <?=$tree->getChecked($ingredient->parrent_id)?>></td>
            <?php } ?>
        </tr>
    <?php }?>
    </tbody>
</table>

<div class="form-group">
    <input type="button" class="btn btn-primary" id="ingredientsSubmit" value="Add product">
</div>

<?php $form = ActiveForm::end() ?>
