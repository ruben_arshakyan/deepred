<?php

namespace backend\recipe\models;

use common\models\Func;
use Yii;

/**
 * This is the model class for table "cat_rel".
 *
 * @property integer $item_id
 * @property integer $cat_id
 */
class RecipeIngredients extends \yii\db\ActiveRecord
{
    public static $update=null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipe_ingredients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id'], 'required'],
            [['item_id', 'ingredient_id'], 'integer'],
            [['count'], 'string', 'max' => 50],
            [['title_am,title_en,title_ru'], 'string']
        ];
    }


    public function addNew($ingredientsID,$count,$placeId){
        $connection = \Yii::$app->db;

        $connection->createCommand()->insert(self::tableName(), [
            'item_id' => $placeId,
            'count' => $count,
            'ingredient_id' => $ingredientsID,

        ])->execute();

//        $this->hasParent($ingredientsID,$placeId);
    }

    public function edit($id,$count){
        $connection = \Yii::$app->db;
        $connection->createCommand()->update(self::tableName(), ['count' => $count], "`id` = '$id'")->execute();

    }

    public function otherEdit($id,$post){
        $connection = \Yii::$app->db;
        $connection->createCommand()->update(self::tableName(), [
            'title_am' => $post['title_am'],
            'title_en' => $post['title_en'],
            'title_ru' => $post['title_ru'],
            'count' => $post['count']
        ], "`id` = '$id'")->execute();

    }

    public function addNewOther($titleAm,$titleEn,$titleRu,$count,$placeId){

        $connection = \Yii::$app->db;

        $connection->createCommand()->insert(self::tableName(), [
            'item_id' => $placeId,
            'title_am' => $titleAm,
            'title_en' => $titleEn,
            'title_ru' => $titleRu,
            'count' => $count

        ])->execute();
    }

    public function updateItems($cats,$placeId){
        $this->deleteOld($placeId);
        foreach($cats as $one){
            $this->addNew($one,$placeId);
        }

    }

    public static function getAllCats($id){

        $result = self::find()->where(['item_id' => $id])->all();
        return $result;
    }

    public function getProduct($id){
        $product = $this->find()->where(['id' => $id])->one();
        return $product;
    }

    public function deleteOld($placeId){
        $query =  $this->find()->where(['item_id'=>$placeId])->all();
        foreach($query as $one){
            $one->delete();
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'ingredient_id' => 'Ingredient ID',
        ];
    }

    public function recipeProduct($id, $lang)
    {
        $model = $this->find()->where(['item_id' => $id])->all();
        return $model;
    }
}
