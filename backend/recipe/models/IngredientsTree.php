<?php

namespace backend\recipe\models;


use backend\recipe\models\Ingredients;
use backend\recipe\models\RecipeIngredients;
use common\models\Func;

class IngredientsTree extends Ingredients{

    public $ingredientsRel;
    public $ingredients;

    public function __construct($id = null){
        //Func::d($id);
        $this->ingredientsRel = new RecipeIngredients();
        if($id !==null){
            $this->ingredients = $this->ingredientsRel->find()->select('ingredient_id,count')->where(['item_id'=>$id])->asArray()->all();
        }
    }

    public function languages(){

        if(isset($_GET['lang']))
        {
            $lang = $_GET['lang'];
        }
        else
        {
            $lang = 'am';
        }

        $res = $this->find()->where(['lang' => $lang])->all();

        foreach ($res as $value) {
            $cats[$value->parrent_id][] = $value;
        }

//        Func::d($cats);


        $this->_category_arr = !empty($cats) ? $cats : null;
    }

    public function getParents($id){
        $res = $this->find()->select('id')->where(['paret_id'=>$id])->all();
        if($res){

            foreach($res as $one){
                $res[] = $this->getParents($one['id']);

            }
        }else return null;

        return $res;
    }

    public function getIdis($res){
        if(is_array($res)){
            foreach($res as $one){
                if(is_array($one)){
                    $id[] = $this->getIdis($one);
                }elseif($one !== null){
                    $id[] = $one['id'];
                }
            }

            return $id;
        }else{
            return null;
        }
    }

    public function outTree($parent_id, $level) {
        $this->languages();
        $class = '';
        if($level != null)
            $class = 'sub';
        //Func::d($this->_category_arr[1]);
        if (isset($this->_category_arr[$parent_id])) {
            foreach ($this->_category_arr[$parent_id] as $value) {

//                Func::d($value->id);

                $attr = $this->getChecked($value->forlang_id);
//                Func::d($this->cats);
                    echo "<li><label><input type='checkbox' class='flat-red' name='ingredient[]' value='". $value->parrent_id . "' ". $attr. "> " . $value->title . "</label></li>";
                    $this->ids[] = $value->id;

                    $level++;
                    $this->outTree($value->id, $level);
                    $level--;
            }
        }
    }


    public function getChecked($id){
//        Func::d($this->ingredients);
        if(!empty($this->ingredients)){
            foreach($this->ingredients as $one){
                if($one['ingredient_id'] == $id){
                    return 'checked';
                }
            }
        }
        return '';
    }

    public function getCount($id){
//        Func::d($this->ingredients);
        if(!empty($this->ingredients)){
            foreach($this->ingredients as $one){
                if($one['ingredient_id'] == $id){
                    return $one['count'];
                }
            }
        }
        return '';
    }

}