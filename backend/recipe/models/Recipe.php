<?php

namespace backend\recipe\models;

use Yii;
use common\models\Func;
use yii\db\Connection;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\behaviors\Upload;
use yii\db\Expression;
use backend\models\Lang;

use mongosoft\file\UploadImageBehavior;

/**
 * This is the model class for table "Recipe".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $img
 * @property string $date
 * @property integer $cat_id
 * @property string $long
 * @property integer $parrent_id
 */
class Recipe extends ActiveRecord
{
    public static  $update = null;
    public $images;

    public function rules()
    {
        return [
            [['lang'], 'required'],
            [['id', 'cat_id', 'parrent_id'], 'integer'],
            [['description','slug'], 'string'],
            [['title'], 'string', 'max' => 250],
            [['video'], 'string', 'max' => 150],
            [['created_at'], 'string', 'max' => 50],
            [['updated_at'], 'string', 'max' => 50],
            [['lang'], 'string', 'max' => 10],
            [['img'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => ['insert', 'update']],
            [['images'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => ['insert', 'update']],
        ];
    }

    /*
   * @slug
   */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'slug',
                'translit' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => Upload::className(),
                'fileName' => 'img',
                'filePath' => 'recipe/',
                'width' => 370,
                'height' => 370,
                'update' => self::$update
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recipe';
    }

    public function addParentId($id){
        $model = $this->findOne($id);

        $model->parrent_id = $id;
        $model->save();
    }

    public function addPostWhithNewLang($id,$lang){

        $info = $this->find()->where(['id' => $id])->select('updated_at,created_at,img,video,cat_id')->one();

//        Func::d($info->cat_id);

        $connection = \Yii::$app->db;
        $connection->createCommand()->insert($this->tableName(), [
            'parrent_id' => $id,
            'lang' => $lang,
            'img' => $info->img,
            'video' => $info->video,
            'updated_at' => $info->updated_at,
            'created_at' => $info->created_at,
            'cat_id' => $info->cat_id,
        ])->execute();

        $id = Yii::$app->db->getLastInsertID();

        return $this->find()->where(['id' => $id,'lang'=> $lang])->one();
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => '',
            'img' => 'Img',
            'video' => 'Video',
            'created_at' => 'Date',
            'cat_id' => 'Category',
            'lang' => 'Lang',
            'parrent_id' => 'Parrent ID',
            'slug' => 'Slug'
        ];
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
//            Func::d($this);
            $connection = \Yii::$app->db;
            $connection ->createCommand()
                ->update($this->tableName(), ['img' => $this->img], "`parrent_id` = '$this->parrent_id' && `lang` != '$this->lang' ")
                ->execute();

            return true;
        } else {
            return false;
        }
    }
    public function findModelLimit($lang)
    {
        return $this->find()->where(['lang'=>$lang])->limit(4)->orderBy('id desc')->all();
    }

    public function findModelCat($ids)
    {
        $recipes = $this->find()->where(['lang' => Lang::getCurrent()->url, 'parrent_id' => $ids]);
        return $recipes;
    }
    public static function findRecentModel($lang)
    {
        return self::find()->where(['lang' => $lang])->limit(2)->orderBy('id desc')->all();
    }
}
