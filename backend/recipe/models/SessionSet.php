<?php
/**
 * Created by PhpStorm.
 * User: DRM
 * Date: 14.01.2016
 * Time: 11:14
 */

namespace backend\recipe\models;


use yii\web\Session;

class SessionSet
{
    private $session;

    public function __construct()
    {
        $this->session = new Session();
        $this->session->open();
    }

    public function setSession($index,$post)
    {
        $ing = [];
        foreach($post as $ingredient)
        {
            array_push($ing, $ingredient);
        }
        $this->session[$index] = $ing;
    }

    public function getSession($index)
    {
        return $this->session[$index];
    }

}