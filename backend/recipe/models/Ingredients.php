<?php

namespace backend\recipe\models;

use common\models\Func;
use Yii;
use yii\behaviors\TimestampBehavior;
use common\behaviors\Upload;
use yii\db\Expression;

/**
 * This is the model class for table "ingredients".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $img
 * @property string $created_at
 * @property integer $cat_id
 * @property string $lang
 * @property integer $parrent_id
 * @property string $slug
 * @property string $updated_at
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 */
class Ingredients extends \yii\db\ActiveRecord
{

    public $_category_arr;
    public static  $update = null;
    public $images;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['cat_id', 'parrent_id'], 'integer'],
            [['lang'], 'required'],
            [['title', 'slug', 'seo_title', 'seo_keywords', 'seo_description'], 'string', 'max' => 250],
            [['img', 'created_at', 'updated_at'], 'string', 'max' => 50],
            [['video'], 'string', 'max' => 150],
            [['lang'], 'string', 'max' => 10],
//            [['country_of_origin', 'usage_area', 'taste_specification'], 'string', 'max' => 70],
            [['images'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => ['insert', 'update']],
        ];
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'slug',
                'translit' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => Upload::className(),
                'fileName' => 'img',
                'filePath' => 'ingredients/',
                'width' => 270,
                'height' => 270,
                'update' => self::$update
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'img' => 'Img',
            'video' => 'Video',
            'created_at' => 'Created At',
            'cat_id' => 'Cat ID',
            'lang' => 'Lang',
            'parrent_id' => 'Parrent ID',
            'slug' => 'Slug',
            'updated_at' => 'Updated At',
            'seo_title' => 'Seo Title',
            'seo_keywords' => 'Seo Keywords',
            'seo_description' => 'Seo Description',
//            'country_of_origin' => 'Country oof origin',
//            'usage_area' => 'Usage Area',
//            'taste_specification' => 'Taste specification'
        ];
    }

    public function addParentId($id){
        $model = $this->findOne($id);

        $model->parrent_id = $id;
        $model->save();
    }

    public function addPostWhithNewLang($id,$lang){

        $info = $this->find()->where(['id' => $id])->select('updated_at,created_at,img,video,cat_id')->one();

//        Func::d($info->cat_id);

        $connection = \Yii::$app->db;
        $connection->createCommand()->insert($this->tableName(), [
            'parrent_id' => $id,
            'lang' => $lang,
            'img' => $info->img,
            'video' => $info->video,
            'updated_at' => $info->updated_at,
            'created_at' => $info->created_at,
        ])->execute();

        $id = Yii::$app->db->getLastInsertID();

        return $this->find()->where(['id' => $id,'lang'=> $lang])->one();
    }

    public function ins($img, $parrent, $lang)
    {

        $connection = \Yii::$app->db;
        $connection ->createCommand()
            ->update($this->tableName(), ['img' => $img], "`parrent_id` = '$parrent' && `lang` != '$lang' ")
            ->execute();
    }

    public function ingredients($lang)
    {
        $ingredients = $this->find()->where(['lang' => $lang])->select('title,img,parrent_id')->all();
        return $ingredients;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
           // Func::d($this);
            $connection = \Yii::$app->db;
            $connection ->createCommand()
                ->update($this->tableName(), ['img' => $this->img], "`parrent_id` = '$this->parrent_id' && `lang` != '$this->lang' ")
                ->execute();

            return true;
        } else {
            return false;
        }
    }
    public function recipeIngredient($parentID,$lang)
    {
        return $this->find()->select('title')->where(['parrent_id'=>$parentID,'lang'=>$lang])->one()->title;
    }
    public function findModelLimit($lang,$limit)
    {
        return $this->find()->where(['lang'=>$lang])->limit($limit)->all();
    }
    public static function findRecentModel($lang,$limit)
    {
        return self::find()->where(['lang' => $lang])->limit($limit)->orderBy('id desc')->all();
    }

    public function getProducts($id,$lang)
    {
        $ingredients = new RecipeIngredients();
        $ingredient = $ingredients->getAllCats($id);
        $ing = new Ingredients();
        $result = '<ul style="list-style-type: none;">';
        $title = 'title_'.$lang;
        foreach ($ingredient as $item) {
            if($item->ingredient_id != null)
            {
                $result .= '<li>'.$ing->recipeIngredient($item->ingredient_id,$lang).'<a href="/backend/recipe/product/update/'.$item->id.'">Edit</a> <a href="/backend/recipe/product/delete/'.$item->id.'">Delete</a></li>';
            }else{
                $result .= '<li>'.$item->$title.'<a href="/backend/recipe/product/update/'.$item->id.'">Edit</a> <a href="/backend/recipe/product/delete/'.$item->id.'">Delete</a></li>';
            }
        }
        $result .= '</ul>';

        return $result;
    }
}
