<?php

namespace backend\recipe\models;


use backend\recipe\models\RecipeRel;
use common\models\Func;

class CategoryTree extends Categories{

    public $catRel;
    public $cats;

    public function __construct($id = null){
        //Func::d($id);
        $this->catRel = new RecipeRel();
        if($id !==null){
            $this->cats = $this->catRel->find()->select('cat_id')->where(['item_id'=>$id])->asArray()->all();
        }
    }

    public function languages(){

        if(isset($_GET['lang']))
        {
            $lang = $_GET['lang'];
        }
        else
        {
            $lang = 'am';
        }

        $res = $this->find()->where(['lang' => $lang])->all();

        foreach ($res as $value) {
            $cats[$value->paret_id][] = $value;
        }


        $this->_category_arr = !empty($cats) ? $cats : null;
    }

    public function getParents($id){
        $res = $this->find()->select('id')->where(['paret_id'=>$id])->all();
        if($res){

            foreach($res as $one){
                $res[] = $this->getParents($one['id']);

            }
        }else return null;

        return $res;
    }

    public function getIdis($res){
        if(is_array($res)){
            foreach($res as $one){
                if(is_array($one)){
                    $id[] = $this->getIdis($one);
                }elseif($one !== null){
                    $id[] = $one['id'];
                }
            }

            return $id;
        }else{
            return null;
        }
    }

    public function outTree($parent_id, $level) {
        $this->languages();
        $class = '';
        if($level != null)
            $class = 'sub';

        if (isset($this->_category_arr[$parent_id])) {
            foreach ($this->_category_arr[$parent_id] as $value) {

//                Func::d($value->id);

                $attr = $this->getChecked($value->forlang_id);
//                Func::d($this->cats);
                echo "<ul class='cats ".$class."'>";
                    echo "<li><label><input type='checkbox' class='flat-red' name='category[]' value='". $value->forlang_id . "' ". $attr. "> " . $value->title . "</label></li>";
                    $this->ids[] = $value->id;

                    $level++;
                    $this->outTree($value->id, $level);
                    $level--;
                echo "</ul>";
            }
        }
    }


    public function getChecked($id){
       // Func::d($this->cats);
        if(!empty($this->cats)){
            foreach($this->cats as $one)
                if(in_array($id,$one)){
                    $attr = 'checked';
                    return $attr;
                }
        }

        return '';
    }

}