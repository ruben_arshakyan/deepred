<?php

namespace backend\product\models;

use Yii;
use common\models\Func;
use yii\db\Connection;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\behaviors\Upload;
use yii\db\Expression;

use mongosoft\file\UploadImageBehavior;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $img
 * @property string $date
 * @property integer $cat_id
 * @property string $long
 * @property integer $parent_id
 */
class Product extends ActiveRecord
{
    public static  $update = null;
    public $images;

    public function rules()
    {
        return [
            [['lang'], 'required'],
            [['id','parent_id'], 'integer'],
            [['description','slug'], 'string'],
            [['title'], 'string', 'max' => 250],
            [['created_at'], 'string', 'max' => 50],
            [[ 'updated_at'], 'string', 'max' => 50],
            [['lang'], 'string', 'max' => 10],
            [['img'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => ['insert', 'update']],
            [['images'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => ['insert', 'update']],
        ];
    }

  /*
   * @slug
   */
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'slug',
                'translit' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => Upload::className(),
                'fileName' => 'img',
                'filePath' => 'product/',
                'update' => self::$update
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }


    public function addParentId($id){
        $model = $this->findOne($id);

        $model->parent_id = $id;
        $model->save();
    }

    public function addPostWhithNewLang($id,$lang){
        $connection = \Yii::$app->db;
        $connection->createCommand()->insert(self::tableName(), [
            'parent_id' => $id,
            'lang' => $lang,
            'img'  => $this->img,
            'created_at'  => $this->created_at,
        ])->execute();

        $id = Yii::$app->db->getLastInsertID();

        return $this->find()->where(['id' => $id,'lang'=> $lang])->one();
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => '',
            'img' => 'Img',
            'created_at' => 'Date',
            'cat_id' => 'Category',
            'lang' => 'Lang',
            'parent_id' => 'Parrent ID',
            'slug' => 'Slug'
        ];
    }

     public function beforeSave($insert)
        {
            if (parent::beforeSave($insert)) {
                $connection = \Yii::$app->db;
                $connection	->createCommand()
                    ->update($this->tableName(), ['img' => $this->img], "`parent_id` = '$this->parent_id' && `lang` != '$this->lang' ")
                    ->execute();

                return true;
            } else {
                return false;
            }
        }
}
