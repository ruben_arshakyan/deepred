<?php

namespace backend\product\models;


class CategoryTree extends Categories{

    public $catRel;
    public $cats;

    public function __construct($id = null){
        $this->catRel = new CatRel();
        if($id !==null){
            $this->cats = $this->catRel->find()->select('cat_id')->where(['item_id'=>$id])->asArray()->all();
        }
    }

    public function asd(){
        $res = $this->find()->all();
        $cats = [];
        foreach ($res as $value) {
            $cats[$value->paret_id][] = $value;
        }
        $this->_category_arr = $cats;
    }

    public function getParents($id){
        $res = $this->find()->select('id')->where(['paret_id'=>$id])->all();
        if($res){

            foreach($res as $one){
                $res[] = $this->getParents($one['id']);

            }
        }else return null;

        return $res;
    }

    public function getIdis($res){
        if(is_array($res)){
            foreach($res as $one){
                if(is_array($one)){
                    $id[] = $this->getIdis($one);
                }elseif($one !== null){
                    $id[] = $one['id'];
                }
            }

            return $id;
        }else{
            return null;
        }
    }

    public function outTree($parent_id, $level) {
        $this->asd();
        $class = '';
        if($level != null)
            $class = 'sub';

        if (isset($this->_category_arr[$parent_id])) {
            foreach ($this->_category_arr[$parent_id] as $value) {

                $attr = $this->getChecked($value->id);
                echo "<ul class='cats ".$class."'>";
                echo "<li><label><input type='checkbox' class='flat-red' name='category[]' value='". $value->id . "' ". $attr ."> " . $value->title . "</label></li>";
                $this->ids[] = $value->id;

                $level++;
                $this->outTree($value->id, $level);
                $level--;
                echo "</ul>";
            }
        }
    }


    public function getChecked($id){
        if(!empty($this->cats)){
            foreach($this->cats as $one)
                if(in_array($id,$one)){
                    $attr = 'checked';
                    return $attr;
                }
        }

        return '';
    }
}