<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Func;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'product';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">


    <p>
        <?= Html::a('Create product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'img',
                'label' => 'Image',
                'format' => 'html',
                'value'=>function ($data) {
                    return Html::a(Html::img('@web/upload/product/thumbs/'.$data->img,['width'=>80]),['update', 'id' => $data->id]);
                }

            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value'=>function ($data) {
                    return Html::a($data->title,['update', 'id' => $data->id]);
                }
            ],
            [
                'attribute' => 'description',
                'label' => 'description',
                'format' => 'html',
                'value' => function($data){
                    return Func::getExcerpt($data->description,0,500);
                }
            ],
            'created_at',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
