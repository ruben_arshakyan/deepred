<?php

namespace backend\page\models;

use common\behaviors\Upload;
use common\interfaces\MultiLanguageModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $img
 * @property string $created_at
 * @property string $lang
 * @property integer $parent_id
 * @property string $slug
 * @property string $updated_at
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property mixed update
 */
class Page extends \yii\db\ActiveRecord implements MultiLanguageModel
{

    public static  $update = null;

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'slug',
                'translit' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => Upload::className(),
                'fileName' => 'img',
                'filePath' => 'page/',
                'update' => self::$update,
                'width' => 1920,
                'height' => 500,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['lang'], 'required'],
            [['parent_id'], 'integer'],
            [['title', 'slug', 'seo_title', 'seo_keywords', 'seo_description'], 'string', 'max' => 250],
            [['created_at', 'updated_at'], 'string', 'max' => 50],
            [['img'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => ['insert', 'update']],
            [['lang'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'img' => 'Img',
            'created_at' => 'Created At',
            'lang' => 'Lang',
            'parent_id' => 'Parent ID',
            'slug' => 'Slug',
            'updated_at' => 'Updated At',
            'seo_title' => 'Seo Title',
            'seo_keywords' => 'Seo Keywords',
            'seo_description' => 'Seo Description',
        ];
    }

    public function addParentId($id){
        $model = $this->findOne($id);

        $model->parent_id = $id;
        $model->save();
    }

    public function addPostWhithNewLang($id,$lang){
        $info = $this->find()->where(['id' => $id])->select('updated_at,created_at,img')->one();

        $connection = \Yii::$app->db;
        $connection->createCommand()->insert($this->tableName(), [
            'parent_id' => $id,
            'lang' => $lang,
            'img' => $info->img,
            'updated_at' => $info->updated_at,
            'created_at' => $info->created_at,
        ])->execute();

        $id = Yii::$app->db->getLastInsertID();

        return $this->find()->where(['id' => $id,'lang'=> $lang])->one();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $connection = \Yii::$app->db;
            $connection	->createCommand()
                ->update($this->tableName(), ['img' => $this->img], "`parent_id` = '$this->parent_id' && `lang` != '$this->lang' ")
                ->execute();

            return true;
        } else {
            return false;
        }
    }

}
