<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\page\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Slider';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">


    <p>
        <?= Html::a('Create new slide', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            //'description:ntext',
            [
                'attribute' => 'img',
                'label' => 'Image',
                'format' => 'html',
                'value'=>function ($data) {
                    return Html::a(Html::img('@web/upload/slider/thumbs/'.$data->img,['width'=>80]),['update', 'id' => $data->id]);
                }

            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value'=>function ($data) {
                    return Html::a($data->title,['update', 'id' => $data->id]);
                }
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
