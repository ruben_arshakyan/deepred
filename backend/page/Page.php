<?php
/**
 * Created by PhpStorm.
 * User: Vardan
 * Date: 11/20/2015
 * Time: 12:12 PM
 */

namespace backend\page;


class Page  extends \yii\base\Module
{
    public $controllerNamespace = 'backend\page\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}