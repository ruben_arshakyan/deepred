<?php

namespace backend\models;

use common\models\Func;
use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property integer $statistics
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statistics'], 'required'],
            [['statistics'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'statistics' => 'Statistics',
        ];
    }

    public static function isEnabled(){
        $model = self::find()->select('statistics')->where(['id' => 1])->one();

        return $model->statistics;
    }

    public function changeStatistics($value){
        $connection = \Yii::$app->db;
        $connection	->createCommand()
            ->update($this->tableName(), ['statistics' => $value], "`id` = '1'")
            ->execute();

    }
}
