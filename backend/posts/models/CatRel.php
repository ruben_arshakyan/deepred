<?php

namespace backend\posts\models;

use common\models\Func;
use Yii;

/**
 * This is the model class for table "cat_rel".
 *
 * @property integer $item_id
 * @property integer $cat_id
 */
class CatRel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cat_rel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'cat_id'], 'required'],
            [['item_id', 'cat_id'], 'integer']
        ];
    }


    public function addNew($catId,$placeId){
        $connection = \Yii::$app->db;

        $connection->createCommand()->insert(self::tableName(), [
            'item_id' => $placeId,
            'cat_id' => $catId,
        ])->execute();

        $this->hasParent($catId,$placeId);
    }

    public function updateItems($cats,$placeId){
        $this->deleteOld($placeId);
        foreach($cats as $one){
            $this->addNew($one,$placeId);

        }

    }

    public static  function getAllCats($id){


        $result = self::find()->select('cat_id')->where(['item_id' => $id])->all();

        foreach ($result as $one) {
            $cats[] = $one->cat_id;
        }

        return $cats;

    }

    public function deleteOld($placeId){
        $query =  $this->find()->where(['item_id'=>$placeId])->all();
        foreach($query as $one){
            $one->delete();
        }

    }


    public function hasParent($catId,$placeId){
        $model = new Categories();

        $result = $model->find()->select('paret_id')->where(['forlang_id'=>$catId])->asArray()->one();

       // Func::d($result);
        if($result['paret_id'] !=0)
            $this->addNew($result['paret_id'],$placeId);
        else
            return;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'cat_id' => 'Cat ID',
        ];
    }
}
