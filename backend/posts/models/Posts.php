<?php

namespace backend\posts\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\behaviors\Upload;
use yii\db\Expression;


/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $img
 * @property string $created_at
 * @property string $lang
 * @property integer $parrent_id
 * @property string $slug
 * @property string $updated_at
 * @property string $publish_date_text
 */
class Posts extends \yii\db\ActiveRecord
{

    public static  $update = null;
    public $images;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
//            [['lang'], 'required'],
//            [['parrent_id'], 'integer'],
            [['title', 'slug', 'publish_date_text'], 'string', 'max' => 250],
            [['created_at', 'updated_at'], 'string', 'max' => 50],
            [['lang'], 'string', 'max' => 10],
            [['img'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => ['insert', 'update']],
        ];
    }


    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'slug',
                'translit' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => Upload::className(),
                'fileName' => 'img',
                'filePath' => 'posts/',
                'update' => self::$update
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'img' => 'Img',
            'created_at' => 'Created At',
            'lang' => 'Lang',
            'parrent_id' => 'Parrent ID',
            'slug' => 'Slug',
            'updated_at' => 'Updated At',
            'publish_date_text' => 'Publish Date Text',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $connection = \Yii::$app->db;
            $connection	->createCommand()
                ->update($this->tableName(), ['img' => $this->img], "`parrent_id` = '$this->parrent_id' && `lang` != '$this->lang' ")
                ->execute();

            return true;
        } else {
            return false;
        }
    }

}
