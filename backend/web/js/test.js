var $ = jQuery.noConflict();
$(document).ready(function($) {

    var gdpData = {
        "IT": 200,
        "ET": 200,
        "AM": 200,
        "CN": 200,
        "VN": 200,
        "IN": 200,
        "DE": 200,
        "PK": 200,
        "AE": 200,
        "GE": 200,
        "GT": 200,
        "ID": 200,
        "MX": 200,
        "UA": 200,
        "AR": 200,
        "CA": 200,
        "SY": 200,
        "TH": 200,
        "US": 200,
    };

    $('#world-map-test').vectorMap({
        map: 'world_mill_en',
        backgroundColor: "transparent",
        zoomOnScroll: false,
        regionStyle: {
            initial: {
                fill: '#d0d0d0',
                "fill-opacity": 1,
                stroke: 'none',
                "stroke-width": 0,
                "stroke-opacity": 1
            }
        },
        series: {
            regions: [{
                values: gdpData,
                scale: ['#a5fb92', '#63af52'],
                normalizeFunction: 'polynomial'
            }]
        },
        onRegionLabelShow: function (e, el, code) {
            el.html(el.html());
        }
    });
});
