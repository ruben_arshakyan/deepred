<?php
/**
 * Created by PhpStorm.
 * User: Vardan
 * Date: 1/15/2016
 * Time: 2:54 PM
 */

namespace backend\controllers;


use backend\models\Settings;
use common\models\Func;
use yii\web\Controller;

class SettingController extends Controller
{

    private $model;

    public function init(){
        $this->model = new Settings();
    }

    public function actionStatistics(){
        $parametr = $_POST['id'] == "true" ? 1 : 0;
        $this->model->changeStatistics($parametr);
    }


}