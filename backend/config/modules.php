<?php
return [
    'posts'=>[
        'class' => 'backend\posts\Posts',
    ],
    'generator' => [
        'class' => 'backend\generator\Generator',
    ],
    'product' => [
        'class' => 'backend\product\Product',
    ],
    'page' => [
        'class' => 'backend\page\Page',
    ],
];