<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
        'multiple' => true,
    ]
]); ?>
<div class="row">
    <div class="form-group col-lg-12">
        <div class="box box-success collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Show gallery</h3>

            </div><!-- /.box-header -->
            <div class="box-body row albom" style="display: block;">
                <div class="col-lg-12">
                    <?= $form->field($model, 'moduleName')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'controllerName')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'modelName')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model,'catDb')->textInput(['maxlength' => true]) ?>
                </div>
            </div><!-- /.box-body -->
        </div>
    </div>

</div>

<div class="col-lg-12">
    <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

