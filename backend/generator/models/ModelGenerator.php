<?php

namespace backend\generator\models;


use Yii;
use yii\helpers\BaseFileHelper;
use common\models\Func;
use backend\generator\models\Generator;
use yii\db\Query;



class ModelGenerator extends Generator{

    protected $modelFullName;
    protected $dbName;
    protected $modelDirectory;
    protected $modelName;
    protected $catDb;
    protected $catRel;

    const DBNAME = "newcms";
    const DBUSER = "root";
    const DBPASS = "";


    public function _set($modelName,$directory,$namespace,$catDb,$catRel){
        $this->modelFullName = ucfirst($modelName).'.php';
        $this->modelName = ucfirst($modelName);
        $this->modelDirectory = $directory.'/models/';
        $this->dbName = $modelName;
        $this->namespace = $namespace;
        $this->catDb = $catDb;
        $this->catRel = $catRel;

    }

    public function generateModel(){
        BaseFileHelper::createDirectory($this->modelDirectory, 509, true);

        $content = $this->modelInner();

        $modelFullName = $this->modelDirectory . $this->modelFullName;
        $myFail = fopen($modelFullName, "w");
        fwrite($myFail,$content);
        fclose($myFail);

        return true;
    }

    public function modelInner(){
        $directory = Yii::getAlias('@backend/generator/templates/model.txt');
        $file = file_get_contents($directory);

        $file = str_replace('@moduleNamespace',$this->namespace,$file);
        $file = str_replace('@modelName',$this->modelName,$file);
        $file = str_replace('@dbname',$this->dbName,$file);

        return $file;
    }



    public function generateCatModel(){
        $content = $this->getCatInner();

        $modelFullName = $this->modelDirectory .'Categories.php';
        $myFail = fopen($modelFullName, "w");
        fwrite($myFail,$content);
        fclose($myFail);

        return true;
    }

    protected function getCatInner(){
        $directory = Yii::getAlias('@backend/generator/templates/catmodel.txt');
        $file = file_get_contents($directory);

        $file = str_replace('@moduleNamespace',$this->namespace,$file);
        $file = str_replace('@dbName',$this->catDb,$file);

        return $file;
    }


    public function generateCatTree(){
        $content = $this->getCatTreeInner();

        $modelFullName = $this->modelDirectory .'CategoryTree.php';
        $myFail = fopen($modelFullName, "w");
        fwrite($myFail,$content);
        fclose($myFail);

        return true;
    }

    public function generateCatRel(){
        $directory = Yii::getAlias('@backend/generator/templates/catRel.txt');
        $file = file_get_contents($directory);

        $file = str_replace('@moduleNamespace',$this->namespace,$file);
        $file = str_replace('@dbName',$this->catRel,$file);

        $modelFullName = $this->modelDirectory .'CatRel.php';
        $myFail = fopen($modelFullName, "w");
        fwrite($myFail,$file);
        fclose($myFail);

        return true;
    }

    protected function getCatTreeInner(){
        $directory = Yii::getAlias('@backend/generator/templates/categorytree.txt');
        $file = file_get_contents($directory);

        $file = str_replace('@moduleNamespace',$this->namespace,$file);

        return $file;
    }

    public function addTable(){
        $table = $this->dbName;
        $sql = "CREATE TABLE IF NOT EXISTS `$table` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `title` varchar(250) DEFAULT NULL,
                  `description` text,
                  `img` varchar(50) DEFAULT NULL,
                  `created_at` varchar(50) DEFAULT NULL,
                  `lang` varchar(10) NOT NULL,
                  `parent_id` int(11) DEFAULT NULL,
                  `slug` varchar(250) DEFAULT NULL,
                  `updated_at` varchar(50) DEFAULT NULL,
                  `seo_title` varchar(250) DEFAULT NULL,
                  `seo_keywords` varchar(250) DEFAULT NULL,
                  `seo_description` varchar(250) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";



        $db = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname='.self::DBNAME,
            'username' => self::DBUSER,
            'password' => self::DBPASS,
            'charset' => 'utf8',
        ]);
        $db->open();

        $command = $db->createCommand($sql);
        $command->execute();
    }


    public function addCatTable(){
        $table = $this->catDb;
        $sql = "CREATE TABLE IF NOT EXISTS `$table` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `slug` varchar(250) NOT NULL,
                  `title` varchar(250) CHARACTER SET armscii8 NOT NULL,
                  `lang` varchar(250) CHARACTER SET armscii8 DEFAULT NULL,
                  `paret_id` int(11) NOT NULL,
                  `img` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
                  `description` text CHARACTER SET armscii8,
                  `forlang_id` int(11) NOT NULL,
                  `seo_title` varchar(250) DEFAULT NULL,
                  `seo_keywords` varchar(250) DEFAULT NULL,
                  `seo_description` varchar(250) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";



        $db = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname='.self::DBNAME,
            'username' => self::DBUSER,
            'password' => self::DBPASS,
            'charset' => 'utf8',
        ]);
        $db->open();

        $command = $db->createCommand($sql);
        $command->execute();
    }


    public function addCatRelTable(){
        $table = $this->catRel;
        $sql = "CREATE TABLE IF NOT EXISTS `$table` (
                  `cat_id` int(11) NOT NULL,
                  `item_id` int(11) NOT NULL,
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

        $db = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname='.self::DBNAME,
            'username' => self::DBUSER,
            'password' => self::DBPASS,
            'charset' => 'utf8',
        ]);
        $db->open();

        $command = $db->createCommand($sql);
        $command->execute();
    }

}