<?php

namespace backend\generator\models;


use Yii;
use yii\helpers\BaseFileHelper;
use common\models\Func;
use backend\generator\models\Generator;



class ControllerGenerator extends Generator{

    protected $controllerFullName;
    protected $controllerDirectory;
    protected $modelName;
    protected $controllerName;
    protected $namespace;

    public function _set($modelName,$directory,$namespace,$controllerName){

        $this->controllerFullName = ucfirst($controllerName).'Controller.php';
        $this->controllerDirectory = $directory.'/controllers/';
        $this->modelName = ucfirst($modelName);
        $this->controllerName = ucfirst($controllerName);
        $this->namespace = $namespace;

    }

    public function generateController(){
        BaseFileHelper::createDirectory($this->controllerDirectory, 509, true);
        $content = $this->getInner();

        $modelFullName = $this->controllerDirectory . $this->controllerFullName;
        $myFail = fopen($modelFullName, "w");
        fwrite($myFail,$content);
        fclose($myFail);

        return true;
    }

    public function getInner(){
        $directory = Yii::getAlias('@backend/generator/templates/controller.txt');
        $file = file_get_contents($directory);

        $file = str_replace('@moduleNamespace',$this->namespace,$file);
        $file = str_replace('@modelName',$this->modelName,$file);
        $file = str_replace('@controllerName',$this->controllerName.'Controller',$file);

        return $file;
    }

    public function generateCategory(){
        $content = $this->getCatInner();

        $modelFullName = $this->controllerDirectory . "CategoriesController.php";
        $myFail = fopen($modelFullName, "w");
        fwrite($myFail,$content);
        fclose($myFail);

        return true;
    }

    public function getCatInner(){
        $directory = Yii::getAlias('@backend/generator/templates/categoryController.txt');
        $file = file_get_contents($directory);

        $file = str_replace('@moduleNamespace',$this->namespace,$file);
        $file = str_replace('@modelName',$this->modelName,$file);
        $file = str_replace('@controllerName',$this->controllerName.'Controller',$file);

        return $file;
    }
}