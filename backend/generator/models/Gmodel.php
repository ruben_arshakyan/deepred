<?php

namespace backend\generator\models;

use Yii;
use common\models\Func;
use yii\base\Model;
use yii\db\Connection;
use yii\db\ActiveRecord;
use yii\db\Expression;



class Gmodel extends Model
{
    public $moduleName;
    public $controllerName;
    public $modelName;
    public $catDb;

    public function rules()
    {
        return [
            [['moduleName','controllerName','modelName','catDb'], 'string'],
            [['moduleName','controllerName','modelName','catDb'], 'required'],
        ];
    }



}