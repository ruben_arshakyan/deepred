<?php

namespace backend\generator\models;


use Yii;
use yii\helpers\BaseFileHelper;
use common\models\Func;
use backend\generator\models\Generator;



class ViewGenerator extends Generator
{
    protected $namespace;
    protected $controllerName;
    protected $directory;
    protected $directoryCat;


    public function _set($namespace,$controllerName,$moduleDirectory){
        $this->namespace = $namespace;
        $this->controllerName = $controllerName;
        $this->directory = $moduleDirectory.'/views/'.$this->controllerName.'/';
        $this->directoryCat = $moduleDirectory.'views/categories/';
    }

    public function generateView(){
        BaseFileHelper::createDirectory($this->directory, 509, true);
        BaseFileHelper::createDirectory($this->directoryCat, 509, true);

        $views['item']['_form'] =  Yii::getAlias('@backend/generator/templates/views/item/_form.txt');
        $views['item']['create'] =  Yii::getAlias('@backend/generator/templates/views/item/create.txt');
        $views['item']['index'] =  Yii::getAlias('@backend/generator/templates/views/item/index.txt');
        $views['item']['update'] =  Yii::getAlias('@backend/generator/templates/views/item/update.txt');

        $views['cat']['_form'] =  Yii::getAlias('@backend/generator/templates/views/category/_form.txt');
        $views['cat']['create'] =  Yii::getAlias('@backend/generator/templates/views/category/create.txt');
        $views['cat']['_create'] =  Yii::getAlias('@backend/generator/templates/views/category/_create.txt');
        $views['cat']['categories'] =  Yii::getAlias('@backend/generator/templates/views/category/categories.txt');
        $views['cat']['update'] =  Yii::getAlias('@backend/generator/templates/views/category/update.txt');

        foreach($views['item'] as $key => $value){
            $content = $this->modelInner($value);

            $modelFullName = $this->directory . $key.'.php';
            $myFail = fopen($modelFullName, "w");
            fwrite($myFail,$content);
            fclose($myFail);
        }

        foreach($views['cat'] as $key => $value){
            $content = $this->modelInner($value);

            $modelFullName = $this->directoryCat . $key.'.php';
            $myFail = fopen($modelFullName, "w");
            fwrite($myFail,$content);
            fclose($myFail);
        }


        return true;
    }

    public function modelInner($directory){

        $file = file_get_contents($directory);

        $file = str_replace('@moduleNamespace',$this->namespace,$file);
        $file = str_replace('@controllerName',$this->controllerName,$file);

        return $file;
    }


}