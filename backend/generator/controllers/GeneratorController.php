<?php


namespace backend\generator\controllers;

use Yii;
use backend\generator\models\Gmodel;
use yii\web\Controller;
use backend\generator\models\Generator;
use backend\generator\models\ModelGenerator;
use common\models\Func;

class GeneratorController extends Controller
{
    public $generator;
    public $model;

    public function init(){
        $this->generator = new Generator();
        $this->model = new Gmodel();
    }

    public function actionIndex(){
        if($this->model->load(Yii::$app->request->post())){


            $this->generator ->_set(
                $this->model->moduleName,
                $this->model->controllerName,
                $this->model->modelName,
                $this->model->catDb
            );


            $this->generator->startAll();

        }

        return $this->render('index',[
            'model' => $this->model,
        ]);

    }
}