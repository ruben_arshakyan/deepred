-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 15 2016 г., 10:42
-- Версия сервера: 5.6.16
-- Версия PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `newcms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `rol` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`, `rol`) VALUES
('admin', '2', NULL, 1),
('sooperadmin', '4', NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, NULL, NULL),
('create-page', 1, NULL, NULL, NULL, NULL, NULL),
('create-post', 1, NULL, NULL, NULL, NULL, NULL),
('sooperadmin', 2, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('sooperadmin', 'admin'),
('admin', 'create-post');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(250) NOT NULL,
  `title` varchar(250) CHARACTER SET armscii8 NOT NULL,
  `lang` varchar(250) CHARACTER SET armscii8 DEFAULT NULL,
  `paret_id` int(11) NOT NULL,
  `img` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
  `description` text CHARACTER SET armscii8,
  `forlang_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `slug`, `title`, `lang`, `paret_id`, `img`, `description`, `forlang_id`) VALUES
(30, 'blog', 'Blog', 'am', 0, 'image_1450783843.jpg', '', 30),
(31, 'animals', 'Animals', 'am', 30, 'image_1441972259.jpg', '', 31),
(32, 'dogs', 'Dogs', 'am', 31, NULL, '', 32),
(33, 'all', 'All', 'am', 30, 'image_1454330167.jpg', '', 33);

-- --------------------------------------------------------

--
-- Структура таблицы `cat_rel`
--

CREATE TABLE IF NOT EXISTS `cat_rel` (
  `cat_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Дамп данных таблицы `cat_rel`
--

INSERT INTO `cat_rel` (`cat_id`, `item_id`, `id`) VALUES
(30, 26, 11),
(30, 27, 46),
(31, 27, 47),
(30, 27, 48),
(30, 28, 89),
(30, 7, 100),
(32, 31, 101),
(31, 31, 102),
(30, 31, 103),
(30, 32, 107),
(31, 32, 108),
(30, 32, 109),
(31, 33, 110),
(30, 33, 111),
(30, 34, 112),
(30, 37, 113),
(30, 43, 119),
(30, 40, 126),
(31, 40, 127),
(30, 40, 128);

-- --------------------------------------------------------

--
-- Структура таблицы `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `body` text NOT NULL,
  `user_ip` varchar(50) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `is_new` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `emails`
--

INSERT INTO `emails` (`id`, `name`, `email`, `subject`, `body`, `user_ip`, `created_at`, `is_new`) VALUES
(2, 'asdasd', 'assd@sad.fg', 'asd', 'asd', '127.0.0.1', '1434708582', 1),
(3, 'asda', 'asd@asdsd.sd', 'asd', 'asd', '127.0.0.1', '1434709054', 1),
(6, 'sfsdf', 'ssfd@sd.hg', 'asdsd', 'ssds', '127.0.0.1', '1434712505', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` text,
  `created_at` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `event`
--

INSERT INTO `event` (`id`, `title`, `description`, `created_at`) VALUES
(7, 'asdasd', '', '2015-06-15'),
(10, '7', 'fgh', '2015-06-07'),
(11, '14', 'sdfsdf', '2015-06-11 20:25'),
(12, '17', 'fgjghj', '2015-06-16'),
(13, 'asdasd', '', '2015-06-26 18:50'),
(14, 'asdads', 'asdsada  ', '2015-06-26 19:45');

-- --------------------------------------------------------

--
-- Структура таблицы `lang`
--

CREATE TABLE IF NOT EXISTS `lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `local` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `default` smallint(6) NOT NULL DEFAULT '0',
  `date_update` int(11) NOT NULL,
  `date_create` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `lang`
--

INSERT INTO `lang` (`id`, `url`, `local`, `name`, `default`, `date_update`, `date_create`) VALUES
(1, 'en', 'en-EN', 'English', 0, 1430754453, 1430754453),
(2, 'ru', 'ru-RU', 'Русский', 0, 1431428627, 1430754453),
(3, 'am', 'am-AM', 'Հայերեն', 1, 1431428613, 1430901563);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1433340074),
('m130524_201442_init', 1433340078);

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `img` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8 NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `updated_at` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `seo_title` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `seo_keywords` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `seo_description` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `img` varchar(50) DEFAULT NULL,
  `created_at` varchar(50) DEFAULT NULL,
  `lang` varchar(10) NOT NULL,
  `parrent_id` int(11) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `title`, `description`, `img`, `created_at`, `lang`, `parrent_id`, `slug`, `updated_at`) VALUES
(40, 'hjkghjkghjk', '', 'image_1454315972.jpg', '2016-02-01 12:39:18', 'am', 40, 'hjkghjkghjk', '2016-02-04 10:34:47'),
(41, NULL, NULL, 'image_1454315972.jpg', '2016-02-01 12:39:18', 'ru', 40, NULL, NULL),
(42, '', '', 'image_1454315972.jpg', '2016-02-01 12:39:18', 'en', 40, '', '2016-02-01 12:39:32'),
(43, 'hjkghjkghjk', '', 'image_1454337342.jpg', '2016-02-01 18:35:43', 'am', 43, 'hjkghjkghjk-2', '2016-02-01 18:35:43'),
(44, 'hjkghjkghjk', '', 'image_1454399116.jpg', '2016-02-02 11:45:16', 'am', 44, 'hjkghjkghjk-3', '2016-02-02 11:45:16');

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `img` varchar(50) DEFAULT NULL,
  `created_at` varchar(50) DEFAULT NULL,
  `lang` varchar(10) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL,
  `seo_title` varchar(250) DEFAULT NULL,
  `seo_keywords` varchar(250) DEFAULT NULL,
  `seo_description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `title`, `description`, `img`, `created_at`, `lang`, `parent_id`, `slug`, `updated_at`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(1, 'lorem ipsum', '', 'image_1454412884.jpg', '2016-02-02 11:51:24', 'am', 1, 'lorem-ipsum', '2016-02-02 15:34:45', NULL, NULL, NULL),
(2, 'asd', '', 'image_1454412884.jpg', '2016-02-02 11:51:24', 'ru', 1, 'asd', '2016-02-02 11:52:39', NULL, NULL, NULL),
(3, 'sdf', '', 'image_1454412884.jpg', '2016-02-02 11:51:24', 'en', 1, 'sdf', '2016-02-02 16:07:45', NULL, NULL, NULL),
(4, 'asd', '', 'image_1454400666.jpg', '2016-02-02 11:57:27', 'am', 4, 'asd-2', '2016-02-02 12:11:06', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `product_cat`
--

CREATE TABLE IF NOT EXISTS `product_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(250) NOT NULL,
  `title` varchar(250) CHARACTER SET armscii8 NOT NULL,
  `lang` varchar(250) CHARACTER SET armscii8 DEFAULT NULL,
  `paret_id` int(11) NOT NULL,
  `img` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
  `description` text CHARACTER SET armscii8,
  `forlang_id` int(11) NOT NULL,
  `seo_title` varchar(250) DEFAULT NULL,
  `seo_keywords` varchar(250) DEFAULT NULL,
  `seo_description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `product_cat`
--

INSERT INTO `product_cat` (`id`, `slug`, `title`, `lang`, `paret_id`, `img`, `description`, `forlang_id`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(1, 'lorem-ipsum', 'lorem ipsum', 'am', 0, '', '', 1, NULL, NULL, NULL),
(2, 'asdasd', 'asdasd', 'am', 3, '', '', 2, NULL, NULL, NULL),
(3, 'new', 'new', 'am', 1, '', '', 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `product_cat_rel`
--

CREATE TABLE IF NOT EXISTS `product_cat_rel` (
  `cat_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Дамп данных таблицы `product_cat_rel`
--

INSERT INTO `product_cat_rel` (`cat_id`, `item_id`, `id`) VALUES
(1, 4, 28),
(1, 1, 40);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statistics` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `statistics`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  `img` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `lang` varchar(10) CHARACTER SET utf8 NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `updated_at` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`id`, `title`, `description`, `img`, `created_at`, `lang`, `parent_id`, `updated_at`) VALUES
(37, 'lorem ipsum', '', 'image_1454503670.jpg', '2016-02-03 16:30:46', 'am', 37, '2016-02-03 16:47:50'),
(38, 'lorem ipsum', '', 'image_1454510404.jpg', '2016-02-03 18:40:05', 'am', 38, '2016-02-03 18:40:05');

-- --------------------------------------------------------

--
-- Структура таблицы `statistics`
--

CREATE TABLE IF NOT EXISTS `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_adress` varchar(50) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  `brauzer` varchar(50) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Дамп данных таблицы `statistics`
--

INSERT INTO `statistics` (`id`, `ip_adress`, `session_id`, `country`, `city`, `date`, `brauzer`, `count`, `platform`) VALUES
(14, '127.0.0.1', 'qv54ejhqk8ubm32hkghrp24vo0', 'AM', '', '2015-06-23 13:06:09.000000', 'Chrome', 4, NULL),
(15, 'asdasd', 'asdasd', 'RU', NULL, '2015-06-24 00:00:00.000000', 'Opera', 1, NULL),
(16, 'asd', 'asd', 'AM', 'asd', '2015-06-23 00:00:00.000000', 'Opera', 1, NULL),
(18, 'sdfsdf', 'sdfsdf', 'RU', NULL, '2015-06-19 00:00:00.000000', NULL, 3, NULL),
(19, 'sdfsdf', 'sdfsdf', 'AM', NULL, '2015-06-19 00:00:00.000000', NULL, 1, NULL),
(21, '127.0.0.1', 'rc0nao9df6dbcagvgkd44hrnt6', 'AM', '', '2015-06-23 17:03:17.000000', 'Chrome', 2, 'Windows'),
(22, '127.0.0.1', 'ssnt5f50gt5hr9iooedrf58o66', '', '', '2015-06-25 15:33:29.000000', 'Chrome', 1, 'Windows'),
(23, '127.0.0.1', 'p82ct9d0277jln8q31p1af9uq2', '', '', '2015-06-29 12:39:38.000000', 'Chrome', 7, 'Windows'),
(24, '127.0.0.1', 'a4e8g6nefmmbbfmfq82qvndv51', '', '', '2015-07-09 15:50:22.000000', 'Chrome', 1, 'Windows'),
(25, '127.0.0.1', 'viegjcf7e71oa6tv77hshgdso6', '', '', '2015-07-10 12:55:57.000000', 'Chrome', 3, 'Windows'),
(26, '127.0.0.1', 'vdpku0s77fpltugs0abfsk6oe7', '', '', '2015-07-20 13:43:33.000000', 'Chrome', 1, 'Windows'),
(27, '127.0.0.1', 'nusr5ik0dc6sdmhvcqpvrkfe65', '', '', '2015-07-29 18:13:34.000000', 'Chrome', 3, 'Windows'),
(28, '127.0.0.1', 'akld5in5ntfabr20sbidmapff6', '', '', '2015-07-29 18:18:43.000000', 'Chrome', 1, 'Windows'),
(29, '127.0.0.1', '920j3c2jog52l747o6j07lq121', '', '', '2015-08-04 14:52:29.000000', 'Chrome', 1, 'Windows'),
(30, '127.0.0.1', '1q5i0sk5qe8vsk9n7ijul1ak71', '', '', '2015-08-07 11:00:03.000000', 'Chrome', 1, 'Windows'),
(31, '127.0.0.1', '243im38r8c8pl2h4ld1n42fs45', '', '', '2015-08-07 11:09:49.000000', 'Chrome', 1, 'Windows'),
(32, '127.0.0.1', 'kkfgvntn1v7lvbiptqnvgin5v3', '', '', '2015-08-07 11:09:55.000000', 'Chrome', 2, 'Windows'),
(33, '127.0.0.1', 'vldontnv03v62hae0victebil1', '', '', '2015-08-07 11:22:44.000000', 'Chrome', 2, 'Windows'),
(34, '127.0.0.1', 'gf3gic9sae13qv7m1dpm6tkse2', '', '', '2015-08-07 15:26:20.000000', 'Chrome', 1, 'Windows'),
(35, '127.0.0.1', 'oh5gfuovlog0qjbcmn5de1etk2', '', '', '2015-08-10 11:18:42.000000', 'Chrome', 1, 'Windows'),
(36, '127.0.0.1', '430r36a962dmfo1vlmp37ko026', '', '', '2015-08-10 11:21:24.000000', 'Chrome', 1, 'Windows'),
(37, '127.0.0.1', '5e53gfmri6r2r4d0o786e03ug3', '', '', '2015-08-13 16:07:06.000000', '', 1, 'Windows'),
(38, '127.0.0.1', 'e74jjmr604bkshkud6fh0gimm4', '', '', '2015-08-19 18:10:11.000000', 'Chrome', 1, 'Windows'),
(39, '127.0.0.1', 'a22b3edg6l6nrbpsdtp673jgu2', '', '', '2015-09-08 18:11:05.000000', 'Chrome', 1, 'Windows'),
(40, '127.0.0.1', '22i72dg6fv8ep8ho3h5ke41uv6', '', '', '2015-09-09 12:36:03.000000', 'Chrome', 1, 'Windows'),
(41, '127.0.0.1', '7l6ma5c252d3s947qq8l0u6cs2', '', '', '2015-09-10 12:04:56.000000', 'Chrome', 3, 'Windows'),
(42, '127.0.0.1', 'vt4p8hn3sno5fvchic51o5msl1', '', '', '2015-10-08 17:51:28.000000', 'Chrome', 1, 'Windows'),
(43, '127.0.0.1', 'u00rgr9j7r3p01e9f5hs2eija4', '', '', '2015-10-16 11:23:17.000000', 'Chrome', 1, 'Windows'),
(44, '127.0.0.1', 'dmi1iuppjm1kkrqve7ho1gt5m0', '', '', '2015-12-08 11:42:10.000000', '', 1, 'Windows'),
(45, '127.0.0.1', 'rsnoglfk9i9klhn31rtlran1b7', '', '', '2016-01-11 14:14:27.000000', 'Chrome', 4, 'Windows'),
(46, '127.0.0.1', 'r487li4q171qmigkhmfm0bg2o1', '', '', '2016-01-20 12:39:52.000000', 'Firefox', 1, 'Windows'),
(47, '127.0.0.1', 'tifccuq70au07ejiv41boj8gn4', '', '', '2016-01-27 16:42:03.000000', 'Chrome', 1, 'Windows');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lastename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `name`, `lastename`) VALUES
(2, 'admin', 'PGo3XfxbFgkMeuK9NRyGFN8RQL9KQon4', '$2y$13$6ecPSF2UVQKygUPktVIxF.O5CiRA58.9wrQlUQdEAcDrXupaGvL5i', NULL, 'movses.meliksetyan@mail.ru', 10, 1434008185, 1434008185, 'admin', 'admin'),
(4, 'MOVSES777', 'gDARBLZOdxr2r_S2apNo4UkcKeIhyJik', '$2y$13$OWoAtF.3BPTJx6wG3zR85.Vvo8BO9p7cCacUQb39.TIhyx7oHbwvy', NULL, 'movses.meliksetyan1991@gmail.com', 10, 1434793741, 1434793741, 'Movses', 'Meliksetyan');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
