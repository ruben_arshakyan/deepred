
// File: Main Java Script File

// Theme Name: Gilas | Creative Onepage Portfolio Template
// Author: sohayl
// Author URI: http://themeforest.net/user/sohayl/

// Content: 1-Parallax 2-Line-Animation 3-Service-Slider 4-Zero-Opacity 5-Cout-Up-Numbers 6-Animation-Scroll-Reveal 7-Google-Map





// Parallax

(function(e) {
    var t = e(window);
    var n = t.height();
    t.resize(function() {
        n = t.height()
    });
    e.fn.parallax = function(r, i, s) {
        function l() {
            var s = t.scrollTop();
            o.each(function() {
                var t = e(this);
                var f = t.offset().top;
                var l = u(t);
                if (f + l < s || f > s + n) {
                    return
                }
                o.css("backgroundPosition", r + " " + Math.round((a - s) * i) + "px")
            })
        }
        var o = e(this);
        var u;
        var a;
        var f = 0;
        o.each(function() {
            a = o.offset().top
        });
        if (s) {
            u = function(e) {
                return e.outerHeight(true)
            }
        } else {
            u = function(e) {
                return e.height()
            }
        }
        if (arguments.length < 1 || r === null) r = "50%";
        if (arguments.length < 2 || i === null) i = .1;
        if (arguments.length < 3 || s === null) s = true;
        t.bind("scroll", l).resize(l);
        l()
    }
})(jQuery)


jQuery('p:empty').remove();
jQuery('p').each(function() {
    var jQuerythis = jQuery(this);
    if(jQuerythis.html().replace(/\s|&nbsp;/g, '').length == 0){
    	//jQuerythis.css('margin','0px');
       jQuerythis.remove();
    }
});
jQuery('.contact form').attr('name','gilas_contact_form');
// Line Animation Under Text

jQuery(document).ready(function(jQuery) {
    var animationDelay = 2500,
        barAnimationDelay = 6500,
        barWaiting = barAnimationDelay - 6000,
        lettersDelay = 50,
        typeLettersDelay = 150,
        selectionDuration = 500,
        typeAnimationDelay = selectionDuration + 800,
        revealDuration = 600,
        revealAnimationDelay = 1500;

    initHeadline();


    jQuery(".live-tile").liveTile();
    jQuery(".tile-backward .live-tile").liveTile({
        animationDirection: 'backward'
    });
    jQuery(".tile-forward .live-tile").liveTile({
        animationDirection: 'forward'
    });


    function initHeadline() {
        singleLetters(jQuery('.cd-headline.letters').find('b'));
        animateHeadline(jQuery('.cd-headline'));
    }

    function singleLetters(jQuerywords) {
        jQuerywords.each(function() {
            var word = jQuery(this),
                letters = word.text().split(''),
                selected = word.hasClass('is-visible');
            for (i in letters) {
                if (word.parents('.rotate-2').length > 0) letters[i] = '<em>' + letters[i] + '</em>';
                letters[i] = (selected) ? '<i class="in">' + letters[i] + '</i>' : '<i>' + letters[i] + '</i>';
            }
            var newLetters = letters.join('');
            word.html(newLetters).css('opacity', 1);
        });
    }

    function animateHeadline(jQueryheadlines) {
        var duration = animationDelay;
        jQueryheadlines.each(function() {
            var headline = jQuery(this);

            if (headline.hasClass('loading-bar')) {
                duration = barAnimationDelay;
                setTimeout(function() {
                    headline.find('.cd-words-wrapper').addClass('is-loading')
                }, barWaiting);
            } else if (headline.hasClass('clip')) {
                var spanWrapper = headline.find('.cd-words-wrapper'),
                    newWidth = spanWrapper.width() + 10
                spanWrapper.css('width', newWidth);
            } else if (!headline.hasClass('type')) {
                var words = headline.find('.cd-words-wrapper b'),
                    width = 0;
                words.each(function() {
                    var wordWidth = jQuery(this).width();
                    if (wordWidth > width) width = wordWidth;
                });
                headline.find('.cd-words-wrapper').css('width', width);
            };

            setTimeout(function() {
                hideWord(headline.find('.is-visible').eq(0))
            }, duration);
        });
    }

    function hideWord(jQueryword) {
        var nextWord = takeNext(jQueryword);

        if (jQueryword.parents('.cd-headline').hasClass('type')) {
            var parentSpan = jQueryword.parent('.cd-words-wrapper');
            parentSpan.addClass('selected').removeClass('waiting');
            setTimeout(function() {
                parentSpan.removeClass('selected');
                jQueryword.removeClass('is-visible').addClass('is-hidden').children('i').removeClass('in').addClass('out');
            }, selectionDuration);
            setTimeout(function() {
                showWord(nextWord, typeLettersDelay)
            }, typeAnimationDelay);

        } else if (jQueryword.parents('.cd-headline').hasClass('letters')) {
            var bool = (jQueryword.children('i').length >= nextWord.children('i').length) ? true : false;
            hideLetter(jQueryword.find('i').eq(0), jQueryword, bool, lettersDelay);
            showLetter(nextWord.find('i').eq(0), nextWord, bool, lettersDelay);

        } else if (jQueryword.parents('.cd-headline').hasClass('clip')) {
            jQueryword.parents('.cd-words-wrapper').animate({
                width: '2px'
            }, revealDuration, function() {
                switchWord(jQueryword, nextWord);
                showWord(nextWord);
            });

        } else if (jQueryword.parents('.cd-headline').hasClass('loading-bar')) {
            jQueryword.parents('.cd-words-wrapper').removeClass('is-loading');
            switchWord(jQueryword, nextWord);
            setTimeout(function() {
                hideWord(nextWord)
            }, barAnimationDelay);
            setTimeout(function() {
                jQueryword.parents('.cd-words-wrapper').addClass('is-loading')
            }, barWaiting);

        } else {
            switchWord(jQueryword, nextWord);
            setTimeout(function() {
                hideWord(nextWord)
            }, animationDelay);
        }
    }

    function showWord(jQueryword, jQueryduration) {
        if (jQueryword.parents('.cd-headline').hasClass('type')) {
            showLetter(jQueryword.find('i').eq(0), jQueryword, false, jQueryduration);
            jQueryword.addClass('is-visible').removeClass('is-hidden');

        } else if (jQueryword.parents('.cd-headline').hasClass('clip')) {
            jQueryword.parents('.cd-words-wrapper').animate({
                'width': jQueryword.width() + 10
            }, revealDuration, function() {
                setTimeout(function() {
                    hideWord(jQueryword)
                }, revealAnimationDelay);
            });
        }
    }

    function hideLetter(jQueryletter, jQueryword, jQuerybool, jQueryduration) {
        jQueryletter.removeClass('in').addClass('out');

        if (!jQueryletter.is(':last-child')) {
            setTimeout(function() {
                hideLetter(jQueryletter.next(), jQueryword, jQuerybool, jQueryduration);
            }, jQueryduration);
        } else if (jQuerybool) {
            setTimeout(function() {
                hideWord(takeNext(jQueryword))
            }, animationDelay);
        }

        if (jQueryletter.is(':last-child') && jQuery('html').hasClass('no-csstransitions')) {
            var nextWord = takeNext(jQueryword);
            switchWord(jQueryword, nextWord);
        }
    }

    function showLetter(jQueryletter, jQueryword, jQuerybool, jQueryduration) {
        jQueryletter.addClass('in').removeClass('out');

        if (!jQueryletter.is(':last-child')) {
            setTimeout(function() {
                showLetter(jQueryletter.next(), jQueryword, jQuerybool, jQueryduration);
            }, jQueryduration);
        } else {
            if (jQueryword.parents('.cd-headline').hasClass('type')) {
                setTimeout(function() {
                    jQueryword.parents('.cd-words-wrapper').addClass('waiting');
                }, 200);
            }
            if (!jQuerybool) {
                setTimeout(function() {
                    hideWord(jQueryword)
                }, animationDelay)
            }
        }
    }

    function takeNext(jQueryword) {
        return (!jQueryword.is(':last-child')) ? jQueryword.next() : jQueryword.parent().children().eq(0);
    }

    function takePrev(jQueryword) {
        return (!jQueryword.is(':first-child')) ? jQueryword.prev() : jQueryword.parent().children().last();
    }

    function switchWord(jQueryoldWord, jQuerynewWord) {
        jQueryoldWord.removeClass('is-visible').addClass('is-hidden');
        jQuerynewWord.removeClass('is-hidden').addClass('is-visible');
    }

});;






// Service Section Slider

(function() {
    function setEqualHeight(columns) {
        var tallestcolumn = 0;

        columns.each(function() {
            currentHeight = jQuery(this).height();
            if (currentHeight > tallestcolumn) {
                tallestcolumn = currentHeight;
            }
        });

        columns.height(tallestcolumn);
    }

    var delay = (function() {
        var timer = 0;

        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    var app = {
        init: function() {

            jQuery("#services-slider").sudoSlider({
                customLink: 'a.servicesLink',
                speed: 1600,
                responsive: true,
                prevNext: true,
                useCSS: true,
                continuous: true,
                updateBefore: true
            });

        }
    };

    app.init();
    jQuery(function() {

        jQuery(window).load(app.windowLoad);
    });

    jQuery(window).resize(function() {
        delay(function() {
            jQuery('.same-height').css('height', 'auto');
            setEqualHeight(jQuery('.same-height'));
        }, 500);
    });

})(jQuery)







// Zero Opacity Script

function herofn() {
    var st = jQuery(window).scrollTop(),
        hh = jQuery('#home').height();
    if (st <= hh) {
        if (jQuery('.sefr-opacity').length) {
            jQuery('.sefr-opacity').css('opacity', 1 - (10 * st) / (6 * hh))
        }
        if (jQuery('.sefr-fixed').length) {
            jQuery('.page-body').css({
                'margin-top': hh
            });
        }
    }
}
jQuery(window).scroll(function() {
    herofn();
});
jQuery(window).resize(function() {
    herofn();
}).resize();






// Count-Up Numbers

(function(e) {
    function t(e, t) {
        return e.toFixed(t.decimals)
    }
    e.fn.countTo = function(t) {
        t = t || {};
        return e(this).each(function() {
            function l() {
                a += i;
                u++;
                c(a);
                if (typeof n.onUpdate == "function") {
                    n.onUpdate.call(s, a)
                }
                if (u >= r) {
                    o.removeData("countTo");
                    clearInterval(f.interval);
                    a = n.to;
                    if (typeof n.onComplete == "function") {
                        n.onComplete.call(s, a)
                    }
                }
            }

            function c(e) {
                var t = n.formatter.call(s, e, n);
                o.text(t)
            }
            var n = e.extend({}, e.fn.countTo.defaults, {
                from: e(this).data("from"),
                to: e(this).data("to"),
                speed: e(this).data("speed"),
                refreshInterval: e(this).data("refresh-interval"),
                decimals: e(this).data("decimals")
            }, t);
            var r = Math.ceil(n.speed / n.refreshInterval),
                i = (n.to - n.from) / r;
            var s = this,
                o = e(this),
                u = 0,
                a = n.from,
                f = o.data("countTo") || {};
            o.data("countTo", f);
            if (f.interval) {
                clearInterval(f.interval)
            }
            f.interval = setInterval(l, n.refreshInterval);
            c(a)
        })
    };
    e.fn.countTo.defaults = {
        from: 0,
        to: 0,
        speed: 1e3,
        refreshInterval: 100,
        decimals: 0,
        formatter: t,
        onUpdate: null,
        onComplete: null
    }
})(jQuery)








// View Animation Scroll Reveal

! function(t, e) {
    "function" == typeof define && define.amd ? define(e) : "object" == typeof exports ? module.exports = e(require, exports, module) : t.scrollReveal = e()
}(this, function() {
    return window.scrollReveal = function(t) {
        "use strict";

        function e(e) {
            this.docElem = t.document.documentElement, this.options = this.extend(this.defaults, e), this.styleBank = {}, 1 == this.options.init && this.init()
        }
        var i = 1,
            o = function() {
                return t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || function(e) {
                    t.setTimeout(e, 1e3 / 60)
                }
            }();
        return e.prototype = {
            defaults: {
                after: "0s",
                enter: "bottom",
                move: "24px",
                over: "0.66s",
                easing: "ease-in-out",
                opacity: 0,
                viewportFactor: .33,
                reset: !1,
                init: !0
            },
            init: function() {
                this.scrolled = !1;
                var e = this;
                this.elems = Array.prototype.slice.call(this.docElem.querySelectorAll("[data-scroll-reveal]")), this.elems.forEach(function(t) {
                    var o = t.getAttribute("data-scroll-reveal-id");
                    o || (o = i++, t.setAttribute("data-scroll-reveal-id", o)), e.styleBank[o] || (e.styleBank[o] = t.getAttribute("style")), e.update(t)
                });
                var r = function() {
                        e.scrolled || (e.scrolled = !0, o(function() {
                            e._scrollPage()
                        }))
                    },
                    n = function() {
                        function t() {
                            e._scrollPage(), e.resizeTimeout = null
                        }
                        e.resizeTimeout && clearTimeout(e.resizeTimeout), e.resizeTimeout = setTimeout(t, 200)
                    };
                t.addEventListener("scroll", r, !1), t.addEventListener("resize", n, !1)
            },
            _scrollPage: function() {
                var t = this;
                this.elems.forEach(function(e) {
                    t.update(e)
                }), this.scrolled = !1
            },
            parseLanguage: function(t) {
                function e(t) {
                    var e = [],
                        i = ["from", "the", "and", "then", "but", "with"];
                    return t.forEach(function(t) {
                        i.indexOf(t) > -1 || e.push(t)
                    }), e
                }
                var i = t.getAttribute("data-scroll-reveal").split(/[, ]+/),
                    o = {};
                return i = e(i), i.forEach(function(t, e) {
                    switch (t) {
                        case "enter":
                            return void(o.enter = i[e + 1]);
                        case "after":
                            return void(o.after = i[e + 1]);
                        case "wait":
                            return void(o.after = i[e + 1]);
                        case "move":
                            return void(o.move = i[e + 1]);
                        case "ease":
                            return o.move = i[e + 1], void(o.ease = "ease");
                        case "ease-in":
                            return o.move = i[e + 1], void(o.easing = "ease-in");
                        case "ease-in-out":
                            return o.move = i[e + 1], void(o.easing = "ease-in-out");
                        case "ease-out":
                            return o.move = i[e + 1], void(o.easing = "ease-out");
                        case "over":
                            return void(o.over = i[e + 1]);
                        default:
                            return
                    }
                }), o
            },
            update: function(t) {
                var e = this.genCSS(t),
                    i = this.styleBank[t.getAttribute("data-scroll-reveal-id")];
                return null != i ? i += ";" : i = "", t.getAttribute("data-scroll-reveal-initialized") || (t.setAttribute("style", i + e.initial), t.setAttribute("data-scroll-reveal-initialized", !0)), this.isElementInViewport(t, this.options.viewportFactor) ? t.getAttribute("data-scroll-reveal-complete") ? void 0 : this.isElementInViewport(t, this.options.viewportFactor) ? (t.setAttribute("style", i + e.target + e.transition), void(this.options.reset || setTimeout(function() {
                    "" != i ? t.setAttribute("style", i) : t.removeAttribute("style"), t.setAttribute("data-scroll-reveal-complete", !0)
                }, e.totalDuration))) : void 0 : void(this.options.reset && t.setAttribute("style", i + e.initial + e.reset))
            },
            genCSS: function(t) {
                var e, i, o = this.parseLanguage(t);
                o.enter ? (("top" == o.enter || "bottom" == o.enter) && (e = o.enter, i = "y"), ("left" == o.enter || "right" == o.enter) && (e = o.enter, i = "x")) : (("top" == this.options.enter || "bottom" == this.options.enter) && (e = this.options.enter, i = "y"), ("left" == this.options.enter || "right" == this.options.enter) && (e = this.options.enter, i = "x")), ("top" == e || "left" == e) && (o.move = o.move ? "-" + o.move : "-" + this.options.move);
                var r = o.move || this.options.move,
                    n = o.over || this.options.over,
                    s = o.after || this.options.after,
                    a = o.easing || this.options.easing,
                    l = o.opacity || this.options.opacity,
                    u = "-webkit-transition: -webkit-transform " + n + " " + a + " " + s + ",  opacity " + n + " " + a + " " + s + ";transition: transform " + n + " " + a + " " + s + ", opacity " + n + " " + a + " " + s + ";-webkit-perspective: 1000;-webkit-backface-visibility: hidden;",
                    c = "-webkit-transition: -webkit-transform " + n + " " + a + " 0s,  opacity " + n + " " + a + " " + s + ";transition: transform " + n + " " + a + " 0s,  opacity " + n + " " + a + " " + s + ";-webkit-perspective: 1000;-webkit-backface-visibility: hidden;",
                    f = "-webkit-transform: translate" + i + "(" + r + ");transform: translate" + i + "(" + r + ");opacity: " + l + ";",
                    p = "-webkit-transform: translate" + i + "(0);transform: translate" + i + "(0);opacity: 1;";
                return {
                    transition: u,
                    initial: f,
                    target: p,
                    reset: c,
                    totalDuration: 1e3 * (parseFloat(n) + parseFloat(s))
                }
            },
            getViewportH: function() {
                var e = this.docElem.clientHeight,
                    i = t.innerHeight;
                return i > e ? i : e
            },
            getOffset: function(t) {
                var e = 0,
                    i = 0;
                do isNaN(t.offsetTop) || (e += t.offsetTop), isNaN(t.offsetLeft) || (i += t.offsetLeft); while (t = t.offsetParent);
                return {
                    top: e,
                    left: i
                }
            },
            isElementInViewport: function(e, i) {
                var o = t.pageYOffset,
                    r = o + this.getViewportH(),
                    n = e.offsetHeight,
                    s = this.getOffset(e).top,
                    a = s + n,
                    i = i || 0;
                return r >= s + n * i && a >= o || "fixed" == (e.currentStyle ? e.currentStyle : t.getComputedStyle(e, null)).position
            },
            extend: function(t, e) {
                for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
                return t
            }
        }, e
    }(window), scrollReveal
});

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    // some code..
} else {
    window.scrollReveal = new scrollReveal();
}






//// Google Map
//
//window.google = window.google || {};
//google.maps = google.maps || {};
//(function() {
//
//  function getScript(src) {
//    document.write('<' + 'script src="' + src + '"><' + '/script>');
//  }
//
//  var modules = google.maps.modules = {};
//  google.maps.__gjsload__ = function(name, text) {
//    modules[name] = text;
//  };
//
//  google.maps.Load = function(apiLoad) {
//    delete google.maps.Load;
//    apiLoad([0.009999999776482582,[[["http://mt0.googleapis.com/vt?lyrs=m@295000000\u0026src=api\u0026hl=en-US\u0026","http://mt1.googleapis.com/vt?lyrs=m@295000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"m@295000000",["https://mts0.google.com/vt?lyrs=m@295000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/vt?lyrs=m@295000000\u0026src=api\u0026hl=en-US\u0026"]],[["http://khm0.googleapis.com/kh?v=167\u0026hl=en-US\u0026","http://khm1.googleapis.com/kh?v=167\u0026hl=en-US\u0026"],null,null,null,1,"167",["https://khms0.google.com/kh?v=167\u0026hl=en-US\u0026","https://khms1.google.com/kh?v=167\u0026hl=en-US\u0026"]],[["http://mt0.googleapis.com/vt?lyrs=h@295000000\u0026src=api\u0026hl=en-US\u0026","http://mt1.googleapis.com/vt?lyrs=h@295000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"h@295000000",["https://mts0.google.com/vt?lyrs=h@295000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/vt?lyrs=h@295000000\u0026src=api\u0026hl=en-US\u0026"]],[["http://mt0.googleapis.com/vt?lyrs=t@132,r@295000000\u0026src=api\u0026hl=en-US\u0026","http://mt1.googleapis.com/vt?lyrs=t@132,r@295000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"t@132,r@295000000",["https://mts0.google.com/vt?lyrs=t@132,r@295000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/vt?lyrs=t@132,r@295000000\u0026src=api\u0026hl=en-US\u0026"]],null,null,[["http://cbk0.googleapis.com/cbk?","http://cbk1.googleapis.com/cbk?"]],[["http://khm0.googleapis.com/kh?v=85\u0026hl=en-US\u0026","http://khm1.googleapis.com/kh?v=85\u0026hl=en-US\u0026"],null,null,null,null,"85",["https://khms0.google.com/kh?v=85\u0026hl=en-US\u0026","https://khms1.google.com/kh?v=85\u0026hl=en-US\u0026"]],[["http://mt0.googleapis.com/mapslt?hl=en-US\u0026","http://mt1.googleapis.com/mapslt?hl=en-US\u0026"]],[["http://mt0.googleapis.com/mapslt/ft?hl=en-US\u0026","http://mt1.googleapis.com/mapslt/ft?hl=en-US\u0026"]],[["http://mt0.googleapis.com/vt?hl=en-US\u0026","http://mt1.googleapis.com/vt?hl=en-US\u0026"]],[["http://mt0.googleapis.com/mapslt/loom?hl=en-US\u0026","http://mt1.googleapis.com/mapslt/loom?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt?hl=en-US\u0026","https://mts1.googleapis.com/mapslt?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/ft?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/ft?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/loom?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/loom?hl=en-US\u0026"]]],["en-US","US",null,0,null,null,"http://maps.gstatic.com/mapfiles/","http://csi.gstatic.com","https://maps.googleapis.com","http://maps.googleapis.com",null,"https://maps.google.com","https://gg.google.com","http://maps.gstatic.com/maps-api-v3/api/images/","https://www.google.com/maps",0],["http://maps.gstatic.com/maps-api-v3/api/js/20/5","3.20.5"],[3576534920],1,null,null,null,null,null,"",null,null,0,"http://khm.googleapis.com/mz?v=167\u0026",null,"https://earthbuilder.googleapis.com","https://earthbuilder.googleapis.com",null,"http://mt.googleapis.com/vt/icon",[["http://mt0.googleapis.com/vt","http://mt1.googleapis.com/vt"],["https://mts0.googleapis.com/vt","https://mts1.googleapis.com/vt"],null,null,null,null,null,null,null,null,null,null,["https://mts0.google.com/vt","https://mts1.google.com/vt"],"/maps/vt",295000000,132],2,500,[null,"http://g0.gstatic.com/landmark/tour","http://g0.gstatic.com/landmark/config","","http://www.google.com/maps/preview/log204","","http://static.panoramio.com.storage.googleapis.com/photos/",["http://geo0.ggpht.com/cbk","http://geo1.ggpht.com/cbk","http://geo2.ggpht.com/cbk","http://geo3.ggpht.com/cbk"]],["https://www.google.com/maps/api/js/master?pb=!1m2!1u20!2s5!2sen-US!3sUS!4s20/5","https://www.google.com/maps/api/js/widget?pb=!1m2!1u20!2s5!2sen-US"],null,0,0,"/maps/api/js/ApplicationService.GetEntityDetails"], loadScriptTime);
//  };
//  var loadScriptTime = (new Date).getTime();
//
//})();


//Hero Slider

jQuery(function () {
    'use strict';

    if( jQuery('[data-stellar-background-ratio]').length > 0 ){
        setTimeout(function () {
            var st = jQuery(window).scrollTop();
            jQuery(window).scrollTop(st+1);
            setTimeout(function(){
                jQuery(window).scrollTop(st)
            }, 200)
        }, 200);
    };



    function lightHeader () {
        jQuery('header .logo img').attr('src', 'img/logo-light.png');
        jQuery('.menu-icon span').css({ 'background-color': '#fff' });
        if( jQuery('.hero-slider').length ){
            jQuery('.hero-slider').addClass('light-arrows');
        }   
    }
    function darkHeader () {
        jQuery('header .logo img').attr('src', 'img/logo.png');
        jQuery('.menu-icon span').css({ 'background-color': '#333' });
        if( jQuery('.hero-slider').length ){
            jQuery('.hero-slider').removeClass('light-arrows');
        }       
    }

    if( jQuery('#hero.darkbg').length ){ lightHeader() }

    if( jQuery('#hero').length ){
        jQuery(window).scroll(function () {

            var st = jQuery(window).scrollTop(),
                hh = jQuery('#hero').height();

            if(st >= hh){
                jQuery('header').addClass('whitebg');
                darkHeader();
            }else{
                jQuery('header').removeClass('whitebg');
                if( jQuery('#hero.darkbg').length || jQuery('.flex-active-slide.darkbg').length ){
                    lightHeader();
                }
            }

        })
    }else{
        jQuery('header').addClass('whitebg')
    }   


    if( jQuery('.img-slider').length ){
    jQuery('.img-slider').flexslider({
        animation: "slide",
        smoothHeight: true,
        pauseOnAction: false,
        controlNav: true,
        directionNav: true,
        prevText: "<i class='pe pe-7s-angle-left'></i>",
        nextText: "<i class='pe pe-7s-angle-right'></i>"
    });
    }

    jQuery(window).load(function () {
        jQuery('.hero-slider').flexslider({
            controlNav: false,
            directionNav: true,
            prevText: "",
            nextText: "",

            start: function () {
                if( jQuery('.flex-active-slide.darkbg').length && jQuery('header.whitebg').length == 0 ){
                    lightHeader();
                }
            },
            after: function () {
                if( jQuery('.flex-active-slide.darkbg').length && jQuery('header.whitebg').length == 0 ){
                    lightHeader()
                }else{
                    darkHeader()            
                }           
            }
        });
    });

    if( jQuery('.hero-slider').length ){
        jQuery(window).scroll(function () {
            var st = jQuery(window).scrollTop();
            if( st > 10){
                jQuery('.hero-slider').flexslider("pause");
            }
        });
    }
    jQuery(window).resize(function () {
        jQuery('.hero-slider').height( jQuery('#hero').height() ).width( jQuery('#hero').width() );
    }).resize();


    new WOW().init();

})