
function initMap() {
    var myLatLng = {lat: 40.2079726, lng: 44.526347};

    var map = new google.maps.Map(document.getElementById('google-map'), {
        zoom: 3,
        center: myLatLng,
        scrollwheel: false,
        streetViewControl: false,
        zoomControl: true,
        mapTypeControl: false
    });

    var marker = new google.maps.Marker({
        position: {lat: 51.5074, lng: 0.1278},
        map: map,
        title: 'London Representative Office'
    });
    var marker = new google.maps.Marker({
        position: {lat: 55.7558, lng: 37.6173},
        map: map,
        title: 'Moscow Representative Office'
    });
    var marker = new google.maps.Marker({
        position: {lat: 40.2079726, lng: 44.526347},
        map: map,
        title: 'Headquarters'
    });
    var marker = new google.maps.Marker({
        position: {lat: 25.2048, lng: 55.2708},
        map: map,
        title: 'Dubai Representative Office'
    });

    map.set('styles', [
        {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#000000"
                },
                {
                    "lightness": 40
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#000000"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                },
                {
                    "lightness": 17
                }
            ]
        }
    ]);
}

//$(function() {                       //run when the DOM is ready
//    $(".title-on-m").click(function() {  //use a class, since your ID gets mangled
//        jQuery('#see-map').addClass("js-active");      //add the class to the clicked element
//    });
//});

//$(".title-on-m").click(function() {
//    // assumes element with id='button'
//    $("#google-map").hide(1000);
//});


//$(".title-on-m").click(function(){
//    $("#google-map").show("slow", function(){
//        $("#google-map").hide("slow");
//    });
//});
//$(document).ready(function() {
$( ".title-on-m" ).click(function() {
    $( "#google-map" ).slideToggle( "slow" );
//    if($('#google-map').attr('display','none'))
 //   $('#google-map').attr('display','block');
 //   else
 //       $('#google-map').attr('display','none');
//});
});


//if ($('#google-map').is(':visible')) {
//    $('.title-on-m').click(function () {
//        $('#google-map').hide();
//    });
//} else {
//    $('.title-on-m').click(function () {
//        $('#google-map').show();
//    });
//}


