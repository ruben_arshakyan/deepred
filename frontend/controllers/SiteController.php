<?php
namespace frontend\controllers;

use backend\models\OnepageTextDescription;
use backend\page\models\About;
use backend\page\models\Portfolio;
use backend\page\models\PortfolioCat;
use backend\page\models\Testimonials;
use backend\page\models\Clients;
use backend\page\models\Career;
use backend\page\models\Team;
use backend\page\models\Services;
use backend\posts\models\Posts;
use backend\page\models\Slider;
use common\models\Func;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Emails;
use yii\web\Session;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public $sess;

    public function init(){
        $this->sess = new Session();
        $this->sess->open();
    }

    public function actionIndex()
    {
        $model = new ContactForm();

        $slider = Slider::find()->all();
        $OnepageTextDescription = OnepageTextDescription::find()->one();
        $Career = Career::find()->all();
        $Team = Team::find()->all();
        $Services = Services::find()->all();
        $Posts = Posts::find()
        ->limit(2)
        ->all();
        $Testimonials = Testimonials::find()->all();
        $Clients = Clients::find()->all();
        $Portfolio = Portfolio::find()->all();
        $PortfolioCat = PortfolioCat::find()->all();
        $About = About::find()->all();
//        $emails = Emails::find()->one();
        //Func::d($OnepageTextDescription);

        return $this->render('index',[
            'slider' => $slider,
            'onepageoextdescription' => $OnepageTextDescription,
            'career' => $Career,
            'team' => $Team,
            'services' => $Services,
            'posts' => $Posts,
            'testimonials' => $Testimonials,
            'clients' => $Clients,
            'portfolio' => $Portfolio,
            'portfoliocats' => $PortfolioCat,
            'about' => $About,
            'model' => $model
        ]);


    }


    public function actionTest(){
        $this->sess = new Session();


        $this->sess['aasd'] = 'asd';
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $email = new Emails();
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                $email->insertNew($model,Yii::$app->getRequest()->getUserIP());
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }
            return $this->refresh();
        } else {
//            return $this->render('contact', [
//                'model' => $model,
//            ]);
            return $this->redirect('site/index', [
                'model' => $model
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

}
