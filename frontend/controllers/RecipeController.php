<?php
/**
 * Created by PhpStorm.
 * User: DRM
 * Date: 21.01.2016
 * Time: 11:18
 */

namespace frontend\controllers;


use backend\recipe\models\RecipeIngredients;
use backend\recipe\models\RecipeRel;
use frontend\models\SearchRecipe;
use Yii;
use yii\web\Controller;
use backend\models\Lang;
use common\models\Func;
use backend\recipe\models\Recipe;
use yii\data\Pagination;
use backend\ingredients\models\Ingredients;
use yii\helpers\Url;

class RecipeController extends Controller
{

    public $recipes;
    public $lang;
    public $recipe_rel;
    public $recipe_content;

    public function init()
    {
        $this->lang = Lang::getCurrent()->url;
        $this->recipes = new Recipe();
        $this->recipe_rel = new RecipeRel();
        $this->recipe_content = new RecipeIngredients();
    }

    public function actionIndex($id=null)
    {
//        $recent = $this->recipes->findRecentModel($this->lang);
        if($id != null){
            $cats = $this->recipe_rel->catFilter($id);
            $category = [];
            foreach ($cats as $cat) {
                array_push($category,$cat->item_id);
            }
            $model = $this->recipes->findModelCat($category);
        }else{
            $model = $this->findModel();
        }
        $countQuery = clone $model;
        $pages = new Pagination(['totalCount' => $countQuery->count(),'defaultPageSize'=>6]);
        $models = $model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index',[
            'models' => $models,
            'pages' => $pages,
//            'recent' => $recent
        ]);
    }

    public function actionView($id)
    {
        $model = $this->recipes->find()->where(['parrent_id'=>$id, 'lang'=>$this->lang])->one();

        return $this->render('view',[
            'model' => $model,
//            'recent' => $recent
        ]);
    }

    public function actionSearch(){

        $model = new SearchRecipe();

        if ($model->load(Yii::$app->request->post())) {
//            Func::d($model);
            $model = $model->search($model->keyword);
            $countQuery = clone $model;
            $pages = new Pagination(['totalCount' => $countQuery->count(),'defaultPageSize'=>6]);
            $models = $model->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            return $this->render('index',[
                'models' => $models,
                'pages' => $pages,
//            'recent' => $recent
            ]);
        }

    }

    public function findModel()
    {
        $model = $this->recipes->find()->where(['lang' => $this->lang]);
        return $model;
    }

    public static function getProducts($id,$lang)
    {
        $ingredients = new RecipeIngredients();
        $ingredient = $ingredients->getAllCats($id);
        $ing = new Ingredients();
        $title = 'title_'.$lang;
        $result = '';
        $i=1;
//        Func::d($ingredient);
        foreach ($ingredient as $item){
            $product = $ing->recipeIngredient($item->ingredient_id,$lang);
            if($item->ingredient_id != null)
            {
                $result .= '<p><span class="ingredients">' . $i . '</span><a href="'. Url::to(['product/view', 'id' => $item->ingredient_id, 's' => $product->slug]) .'">'.$product->title.'</a>&nbsp;' . $item->count .'</p>';
            }else{
                $result .= '<p><span class="ingredients">' . $i . '</span>'.$item->$title.'</p>';
            }
            $i++;
        }

        return $result;
    }

}