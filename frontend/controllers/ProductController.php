<?php
/**
 * Created by PhpStorm.
 * User: DRM
 * Date: 19.01.2016
 * Time: 17:49
 */

namespace frontend\controllers;


use backend\ingredients\models\Categories;
use backend\ingredients\models\IngredientsRel;
use frontend\models\SearchProduct;
use Yii;
use yii\web\Controller;
use backend\models\Lang;
use common\models\Func;
use backend\ingredients\models\Ingredients;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class ProductController extends Controller
{

    public $lang;
    public $products;
    public $products_rel;

    public function init()
    {
        $this->lang = Lang::getCurrent()->url;
        $this->products = new Ingredients();
        $this->products_rel = new IngredientsRel();
    }

    public function actionIndex($id = null)
    {
        $categoryName = null;
        if($id != null){
            $cats = $this->products_rel->catFilter($id);
            $category = [];
            foreach ($cats as $cat) {
                array_push($category,$cat->item_id);
            }
            $model = $this->products->findModelCat($category);
            $categoryName = Categories::find()->where(['forlang_id' => $id,'lang' => $this->lang])->one()->title;
        }else{
            $model = $this->findModel();
        }
        $countQuery = clone $model;
        $pages = new Pagination(['totalCount' => $countQuery->count(),'defaultPageSize'=>9]);
        $models = $model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index',[
            'model' => $models,
            'pages' => $pages,
            'categoryName' => $categoryName,
        ]);
    }

    public function actionView($id)
    {
//        $recent = $this->products->findRecentModel($this->lang,2);
        $model = $this->getProduct($id);
        return $this->render('view',[
            'model' => $model,
//            'recent' => $recent
        ]);
    }

    public function actionSearch(){
        $model = new SearchProduct();
        $categoryName = null;
        if ($model->load(Yii::$app->request->post())) {
//            Func::d($model);
            $model = $model->search($model->keyword);
            $countQuery = clone $model;
            $pages = new Pagination(['totalCount' => $countQuery->count(),'defaultPageSize'=>9]);
            $models = $model->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            return $this->render('index',[
                'model' => $models,
                'pages' => $pages,
                'categoryName' => $categoryName,
            ]);
        }

    }

    public function findModel()
    {
        $model = $this->products->find()->where(['lang' => $this->lang]);
        return $model;
    }

    public function getProduct($id){
        $model = $this->products->find()->where(['parrent_id'=>$id, 'lang'=>$this->lang])->one();
        if($model != null){
            return $model;
        }else{
            throw new NotFoundHttpException("The requested page does not exist.");
        }
    }
}