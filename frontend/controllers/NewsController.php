<?php
/**
 * Created by PhpStorm.
 * User: DRM
 * Date: 26.01.2016
 * Time: 16:32
 */

namespace frontend\controllers;


use backend\posts\models\Posts;
use yii\web\Controller;
use backend\models\Lang;
use yii\data\Pagination;

class NewsController extends Controller
{
    public $lang;
    public $news;

    public function init()
    {
        $this->lang = Lang::getCurrent()->url;
        $this->news = new Posts();
    }

    public function actionIndex()
    {
        $model = $this->findModel();
        $countQuery = clone $model;
        $pages = new Pagination(['totalCount' => $countQuery->count(),'defaultPageSize'=>5]);
        $models = $model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index',[
            'model' => $models,
            'pages' => $pages
        ]);
    }

    public function actionView($id)
    {
        $model = $this->news->find()->where(['parrent_id'=>$id, 'lang'=>$this->lang])->one();
        return $this->render('view',[
            'model' => $model
        ]);
    }

    public function findModel()
    {
        $model = $this->news->find()->where(['lang' => $this->lang]);
        return $model;
    }

}