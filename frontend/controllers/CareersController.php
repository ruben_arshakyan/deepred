<?php

namespace frontend\controllers;

use backend\models\OnepageTextDescription;
use backend\page\models\Career;
use frontend\models\ContactForm;

class CareersController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new ContactForm();

        $OnepageTextDescription = OnepageTextDescription::find()->one();
        $Career = Career::find()->all();

        return $this->render('index', [
            'onepageoextdescription' => $OnepageTextDescription,
            'career' => $Career,
            'model' => $model
        ]);
    }

}