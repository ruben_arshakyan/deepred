<?php
/**
 * Created by PhpStorm.
 * User: DRM
 * Date: 22.01.2016
 * Time: 13:41
 */

namespace frontend\controllers;


use backend\about\models\About;
use backend\posts\models\Posts;
use common\models\Func;
use yii\web\Controller;
use backend\models\Lang;
use backend\recipe\models\Ingredients;

class AboutUsController extends Controller
{

    public $lang;
    public $news;
    public $about;

    public function init()
    {
        $this->lang = Lang::getCurrent()->url;
        $this->news = new Posts();
        $this->about = new About();
    }

    public function actionIndex()
    {
        $news = $this->news->findModelLimit($this->lang);
        $about = $this->about->getAbout(1,$this->lang);

        return $this->render('index',[
            'lang' => $this->lang,
            'news' => $news,
            'about' => $about
        ]);
    }

}