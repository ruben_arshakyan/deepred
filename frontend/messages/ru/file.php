<?php
return [
    'contacts' => 'контакты',
    'recepies' => 'рецепты',
    'lunch_menu' => 'меню обеда',
    'specialized_fish_store' => 'Специализированный магазин рыбы',
    'about_us' => 'о нас',
    'see_our_location' => 'посмотреть наше местоположение',
    'how_we' => 'Как мы',
    'make_fish' => 'гатовым рыбу',
    'home' => 'дом',
    'sweet_home' => 'милый дом',
];