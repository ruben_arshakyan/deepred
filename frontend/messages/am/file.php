<?php
return [
    'contacts' => 'Հետադարձ կապ',
    'recepies' => 'բաղադրատոմսեր',
    'lunch_menu' => 'լանչ մենյու',
    'specialized_fish_store' => 'մասնագիտացված ձկան խանութ',
    'about_us' => 'Մեր մասին',
    'see_our_location' => 'տեսնել մեր դիրքը',
    'how_we' => 'ինչպես ենք մենք',
    'make_fish' => 'պատրաստում ձուկ',
    'home' => 'տուն',
    'sweet_home' => 'հարազատ տուն',
    'our_recepies' => '<span>մեր</span> բաղադրատոմսերը',
];