<?php
return [
    'contacts' => 'contacts',
    'recepies' => 'recepies',
    'lunch_menu' => 'lunch menu',
    'specialized_fish_store' => 'specialized fish store',
    'about_us' => 'about us',
    'see_our_location' => 'see our location',
    'how_we' => 'How We',
    'make_fish' => 'make fish',
    'home' => 'Home',
    'sweet_home' => 'Sweet Home',
    'our_recepies' => '<span>our</span> recepies',
];