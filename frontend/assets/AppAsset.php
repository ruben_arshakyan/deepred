<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        'css/styles.css',
        'css/jquery.bxslider.css',
        'css/font-awesome.min.css',
        'css/pe-icon-7-stroke.css',
        'css/et-line.css',
        'css/bootstrap.css',
        'css/style.css',
        'css/extra-pages.css',
        'css/owl.carousel.css',
        'css/magnific-popup.css',
        'css/home.css',
        'css/Met-Js.css',
        'css/style-responsive.css',
        'css/animate.css',
        'css/coustom.css',
        'css/css',

    ];
    public $js = [
        'http://html5shim.googlecode.com/svn/trunk/html5.js',
        'js/jquery.js',
        'js/jquery-migrate.min.js',
        'js/jquery.form.min.js',
        'js/scripts.js',
        'js/bootstrap.min.js',
        'js/modernizr.js',
        'js/jquery.sudoslider.min.js',
        'js/jquery.flexslider.js',
        'js/main.js',
        'js/jquery-migrate-1.2.1.min.js',
        'js/jquery.easing.1.3.js',
        'js/SmoothScroll.js',
        'js/jquery.scrollTo.min.js',
        'js/jquery.localScroll.min.js',
        'js/jquery.ba-hashchange.min.js',
        'js/jquery.viewport.mini.js',
        'js/jquery.appear.js',
        'js/jquery.sticky.js',
        'js/jquery.fitvids.js',
        'js/owl.carousel.min.js',
        'js/isotope.pkgd.min.js',
        'js/imagesloaded.pkgd.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/fadeSlideShow.js',
        'js/soh.min.js',
        'js/MetroJs.js',
//        'js/common.js',
//        'js/js',
        'js/stats.js',
//        'js/util.js',
        'js/util.js',
        'js/wow.min.js',
        'js/post-load-more.js',
        'js/map.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'frontend\assets\BowerAsset',
    ];
}
