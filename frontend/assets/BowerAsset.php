<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BowerAsset extends AssetBundle
{
    public $sourcePath  = '@bower';

    public $css = [
        'owl/owl-carousel/owl.carousel.css',
        'wow/css/libs/animate.css',
    ];
    public $js = [
        'owl/owl-carousel/owl.carousel.min.js',
        'wow/dist/wow.min.js',
    ];

}
