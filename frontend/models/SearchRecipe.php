<?php
/**
 * Created by PhpStorm.
 * User: DRM
 * Date: 22.01.2016
 * Time: 16:14
 */

namespace frontend\models;


use backend\models\Lang;
use backend\recipe\models\Recipe;
use yii\base\Model;

class SearchRecipe extends Recipe
{
    public $keyword;

    public function rules()
    {
        return [
            [['keyword'], 'string', 'max' => 50],
        ];
    }
    public function attributeLabels()
    {
        return [
            'keyword' => 'Search'
        ];
    }

    public function search($key)
    {
        $product = $this->find()
                ->where(['LIKE', 'title' , $key])
                ->orWhere(['LIKE', 'description', $key])
                ->andWhere(['lang' => Lang::getCurrent()->url]);
        return $product;

    }
}