<?php
/**
 * Created by PhpStorm.
 * User: DRM
 * Date: 19.01.2016
 * Time: 14:51
 */

namespace frontend\models;

use backend\models\Lang;
use backend\recipe\models\Categories;


class Category
{
    public static function getCategories()
    {
        $model = new Categories();
        $lang = Lang::getCurrent()->url;
        $categories = $model->find()->select('title,forlang_id')->where(['lang' => $lang])->all();
        return $categories;
    }

    public static function getCategoriesProduct(){
        $model = new \backend\ingredients\models\Categories();
        $lang = Lang::getCurrent()->url;
        $categories = $model->find()->select('title, forlang_id')->where(['lang' => $lang])->all();
        return $categories;
    }

}