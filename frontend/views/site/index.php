<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
?>

        <section id="home">
            <section id="hero" class="fullscreen">
                <div class="js-height-full" style="height: 905px;">
                    <div class="hero-slider" style="height: 905px; width: 1663px;">
                        <ul class="slides">
                            <?php foreach ($slider as $value):?>
                            <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                                <div class="slidebg" style="background-image: url(<?= URL::home().'backend/web/upload/slider/thumbs/'.$value['img'];?>);">
                                </div>
                                <div class="container">
                                    <div class="hero-title text-center hbottom">
                                        <h2 class="title font-alt"><?= $value['title'] ?></h2>
                                        <p><?= $value['description'] ?></p>
                                    </div>
                                </div>
                            </li>
                            <?php endforeach; ?>
<!--                            <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">-->
<!--                                <div class="slidebg" style="background-image: url(http://thenordik.com/demo/themeforest/wordpress/gilas/wp-content/themes/gilas-wp-theme/img/bg/004.jpg)">-->
<!--                                </div>-->
<!--                                <div class="container">-->
<!--                                    <div class="hero-title text-center hbottom">-->
<!--                                        <h2 class="title font-alt">Simple &amp; Clean</h2>-->
<!--                                        <p>The Creativity Plus Clean Design makes this theme awesome ...</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </li>-->
                        </ul>
                        <ul class="flex-direction-nav">
                            <li><a class="flex-prev" href="#"></a></li>
                            <li><a class="flex-next" href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </section>
        </section>
       
         
        <section id="about">             
            <section class="small-section parallax-2">
                <div class="container-2">
                    <div class="about-0 margin-tb-0">
                        <p>About<strong> DRM</strong>.</p>
                        <div class="cher-line">
                            <img src="<?=Url::home()?>image/cher-line.png">
                        </div>
                    </div>
                    <div class="ab-t">
                        <h1 class="cd-headline loading-bar"><?= $onepageoextdescription->about_us_text ?></h1>
                    </div>

                    <div class="col-md-3 margin-tb-3">
                        <div class="about-2">
                            <h5><i class="pe-7s-plane pe-4x"></i></h5>
                            <h1><span><?= $about[0]['title'] ?></span><?=$about[0]['description'] ?><br>
                                <a href="/services/index">+ <strong>Read More</strong></a>
                            </h1>
                        </div>
                    </div>

                    <div class="col-md-3 margin-tb-3">
                        <div class="about-2"><h5><i class="pe-7s-sun pe-4x"></i></h5>
                            <h1><span><?= $about[1]['title'] ?></span><?=$about[1]['description'] ?><br>
                                <a href="/services/index">+ <strong>Read More</strong></a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-md-3 margin-tb-3">
                        <div class="about-2">
                            <h5><i class="pe-7s-server pe-4x"></i></h5>
                            <h1><span><?= $about[2]['title'] ?></span><?=$about[2]['description'] ?><br>
                                <a href="/services/index">+ <strong>Read More</strong></a>
                            </h1>
                        </div>
                    </div>
                    <div class="col-md-3 margin-tb-3">
                        <div class="about-2">
                            <h5><i class="pe-7s-refresh-cloud"></i></h5>
                            <h1><span><?= $about[3]['title'] ?></span><?=$about[3]['description'] ?><br>
                                <a href="/services/index">+ <strong>Read More</strong></a>
                            </h1>
                        </div>
                    </div>
                </div>
            </section>

            <section class="small-section parallax-3">
                <div class="col-md-6 col-lg-6">
                </div>
                <div class="col-md-6 col-lg-6 bg-dark-p">
                    <div class="about-1">
                        <p><?= $onepageoextdescription->about_experience_title ?></p>
                        <h1><?= $onepageoextdescription->about_experience_description ?></h1>
                    </div>
                    <div class="tab-pane fade in active" id="two">
                        <div class="row">
                            <div class="sk-1">
                                <article>
                                    <div class="our-skills">
                                        <div class="skill-bar">HTML/CSS/JQUERY
                                            <div class="skill-bar-value" style="width: 80%; display: block;">
                                                <span class="skill-title">80</span>
                                            </div>
                                        </div>
                                        <div class="skill-bar">PHOTOSHOP
                                            <div class="skill-bar-value" style="width: 40%; display: block;">
                                                <span class="skill-title">40</span>
                                            </div>
                                        </div>
                                        <div class="skill-bar">PHP/WORDPRESS
                                            <div class="skill-bar-value" style="width: 90%; display: block;">
                                                <span class="skill-title">90</span>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div><br><br>
                </div>
            </section>
            <section class="small-section-gr more-about">
                <div class="container-2">
                    <div class="xat-sar">
                        <h1>Why Joining DRM?</h1>
                    </div>
                    <div class="col-md-6">
                        <div id="slideshowWrapper">
                            <ul id="slideshow">
                                <?php foreach ($career as $value):?>
                                <li>
                                    <section class="bg-scroll">
                                        <div class="relative">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="work-full-media pdd-1">
                                                        <i class=""></i>
                                                        <h1><?= $value['title'] ?></h1>
                                                        <h2><?= $value['description'] ?></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="mybutton-t small">
                            <a href="/careers/index">
                                <span data-hover="Read More">Read More</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about-image">
                            <div class="image-bg d-bg-c">
                            </div>
                            <?= Html::img('/backend/web/upload/career/thumbs/'.$career[0]['img'], ['width' => 500, 'height' => 660]);?>
                        </div><br><br><br>
                    </div>
                </div>
            </section><br>
        </section>

        <section id="out-team">
            <section class="small-section parallax-4">
                <div class="pattern-2">
                </div>
                <div class="col-md-12 bg-dark-pp">
                    <div class="team-1">
                        <p><strong>Creative</strong> Team.</p>
                        <h1><?= $onepageoextdescription->creative_team_text ?></h1>
                    </div>
                </div>
            </section>
            <div class="title-on-l">
                <img src="<?=Url::home()?>image/on-l.png" alt="">
            </div>
                    <section class="bg-scroll"><br>
                        <div class="container-3 relative">
                            <div class="row">
                                <div class="item-carousel owl-carousel owl-theme">
                                    <?php foreach ($team as $value):?>
                                    <div class="owl-item">
                                        <div class="team-member-item animate-init">
                                            <?= Html::img('/backend/web/upload/team/thumbs/'.$value['img']);?>
                                            <h6><strong><?= $value['title'] ?></strong><span> <?= $value['description'] ?>  </span>
                                                <a href="<?= $value['facebook'] ?>">
                                                    <i class="icon-facebook"></i>
                                                </a>
                                                <a href="<?= $value['twitter'] ?>">
                                                    <i class="icon-twitter"></i>
                                                </a>
                                                <a href="<?= $value['google'] ?>">
                                                    <i class="icon-googleplus"></i>
                                                </a>
                                            </h6>
                                            <h4> <?= $value['name'] ?> </h4>
                                            <h2> <?= $value['speciality'] ?> </h2>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
                                               
         
        <section id="services">
            <section class="small-section parallax-5">
                <div class="col-md-12">
                    <div class="team-1">
                        <p> <strong>Cool</strong> Services.</p>
                        <h1><?= $onepageoextdescription->cool_services_text ?></h1>
                    </div>
                </div>
                <div class="arr">
                    <p><img src="<?=Url::home()?>image/arr.png" alt=""></p>
                </div>
            </section>

            <div class="col-md-4 bg-ser-sec-1 bg-serc-1">
                <div class="sersec-1">
                    <i class="pe-7s-plane pe-4x"></i>
                    <h1><?= $services[0]['title'] ?></h1>
                    <p><?= $services[0]['description'] ?></p>
                </div>
            </div>

            <div class="col-md-4 bg-ser-sec-1 bg-serc-2">
                <div class="sersec-1">
                    <i class="pe-7s-switch pe-4x"></i>
                    <h1><?= $services[1]['title'] ?></h1>
                    <p><?= $services[1]['description'] ?></p>
                </div>
            </div>
            <div class="col-md-4 bg-ser-sec-1 bg-serc-2">
                <div class="sersec-1">
                    <i class="pe-7s-umbrella pe-4x"></i>
                    <h1><?= $services[2]['title'] ?></h1>
                    <p><?= $services[2]['description'] ?></p>
                </div>
            </div>
            <div class="col-md-4 bg-ser-sec-1 bg-serc-4">
                <div class="sersec-1">
                    <i class="pe-7s-study pe-4x"></i>
                    <h1><?= $services[3]['title'] ?></h1>
                    <p><?= $services[3]['description'] ?></p>
                </div>
            </div>
            <div class="col-md-4 bg-ser-sec-1 bg-serc-5">
                <div class="sersec-1">
                    <i class="pe-7s-sun pe-4x"></i>
                    <h1><?= $services[4]['title'] ?></h1>
                    <p><?= $services[4]['description'] ?></p>
                </div>
            </div>
            <div class="col-md-4 bg-ser-sec-1 bg-serc-6">
                <div class="sersec-1">
                    <i class="pe-7s-server pe-4x"></i>
                    <h1><?= $services[5]['title'] ?></h1>
                    <p><?= $services[5]['description'] ?></p>
                </div>
            </div>

            <section class="small-section parallax-6">
                <section id="service-section" class="service-section content-section ha-waypoint services">
                    <div class="grid-container">
                        <div class="services-buttons">
                            <a data-animated="bounceIn" data-rel="1" href="javascript:void(0);" class="servicesLink">
                                <i class="icon-lightbulb"></i>
                            </a>
                            <a data-animated="bounceIn" data-rel="2" href="javascript:void(0);" class="servicesLink">
                                <i class=" icon-beaker "></i>
                            </a>
                            <a data-animated="bounceIn" data-rel="3" href="javascript:void(0);" class="servicesLink">
                                <i class=" icon-strategy"></i>
                            </a>
                        </div>
                        <div class="line-charm">
                            <p>_</p>
                        </div>
                        <div class="container-2 animate-init" data-anim-type="fade-in" data-anim-delay="100">
                            <div id="services-slider" data-animated="bounceIn" class="ten columns offset-by-one">
                                <ul>
                                    <li>
                                        <div class="ser-1">
                                            <h5><?= $onepageoextdescription->other_service_title_1 ?></h5>
                                            <br>
                                            <br>
                                            <h1><?= $onepageoextdescription->other_service_description_1 ?></h1></div>
                                    </li>
                                    <li>
                                        <div class="ser-1">
                                            <h5><?= $onepageoextdescription->other_service_title_2 ?></h5>
                                            <br>
                                            <br>
                                            <h1><?= $onepageoextdescription->other_service_description_2 ?></h1></div>
                                    </li>
                                    <li>
                                        <div class="ser-1">
                                            <h5><?= $onepageoextdescription->other_service_title_3 ?></h5>
                                            <br>
                                            <br>
                                            <h1><?= $onepageoextdescription->other_service_description_3 ?></h1></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </section>
       
         
        <section id="single-blog">
            <section class="page-section-np" id="blog">
                <div class="relative">
                    <div class="col-md-12">
                        <div class="container-2">
                            <div class="blog-1">
                                <h6 class="cd-headline loading-bar"><b>Recent</b> Posts.</h6>
                                <h1><?= $onepageoextdescription->recent_posts_text ?></h1>
                                <br>
                                <br>
                                <div class="cher-line">
                                    <img src="<?=Url::home()?>image/cher-line.png" alt="">
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                    </div>
                    <?php foreach ($posts as $value):?>
                        <div class="col-md-6 bg-blog-pre-1" style="background-image: url(<?= URL::home().'backend/web/upload/posts/thumbs/'.$value['img'];?>);">
                            <div class="blog-pre-1">
                                <h2><?= $value['publish_date_text'] ?></h2>
                                <h1><?= $value['title'] ?></h1>
                                <!--                            <p>Collection Mock-Ups </p>-->
                            </div>
                        </div>
                        <div class="col-md-6 bg-blog-pre-2">
                            <div class="blog-pre-2">
                                <img src="<?=Url::home()?>image/blog-avatar.png" alt="">
                                <p><?= $value['description'] ?></p>
                                <a href="blog/read/<?= $value->id ?>">
                                    <h1 class="animate-init">Read More ...</h1>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
<!--                    <div class="col-md-6 bg-blog-pre-2222">-->
<!--                        <div class="blog-pre-1">-->
<!--                            <h2><span>17</span>September</h2>-->
<!--                            <h1>Simple Professional<br> Brandi<span>ng</span></h1>-->
<!--                            <p>Collection Mock-Ups </p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-6 bg-blog-pre-2">-->
<!--                        <div class="blog-pre-2">-->
<!--                            <img src="--><?//=Url::home()?><!--image/blog-avatar.png" alt="">-->
<!--                            <p>Donec erat risus, scelerisque quis dapibus viverra, luctus et ligula. Etiam dignissim quam sed leo gravida sagittis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>-->
<!--                            <a href="#">-->
<!--                                <h1 class="animate-init">Read More ...</h1>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
                <div class="updatePost"
                </div>
            </section>
            <div class="more-p">
                <a href="blog/index" class="more">
                    <div class="fact-sec">
                        <h2><img src="<?=Url::home()?>image/plus.png" alt=""></h2>
                        <br>
                        <h3>Load More Projects</h3>
                    </div>
                </a>
            </div>

            <section class="small-section parallax-7">
                <br>
                <br>
                <br>
                <div class="container relative">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-icon">
                                <span class="icon-quote"></span>
                            </div>
                            <div class="single-carousel black owl-carousel owl-theme">
                                <?php foreach ($testimonials as $value):?>
                                        <div class="owl-item">
                                            <div>
                                                <blockquote class="testimonial"><?= $value['title'] ?></blockquote>
                                                <div class="testimonial-author">
                                                    <?= Html::img('/backend/web/upload/testimonials/thumbs/'.$value['img']);?>
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach; ?>
<!--                                        <div class="owl-item">-->
<!--                                            <div>-->
<!--                                                <blockquote class="testimonial"> Design Should Never Say, 'Look at me', It Should Always Say, 'Look at this'</blockquote>-->
<!--                                                <div class="testimonial-author">-->
<!--                                                    <img src="--><?//=Url::home()?><!--image/sign-2.png" alt="">-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="owl-item">-->
<!--                                            <div>-->
<!--                                                <blockquote class="testimonial"> Design Should Never Say, 'Look at me', It Should Always Say, 'Look at this'</blockquote>-->
<!--                                                <div class="testimonial-author">-->
<!--                                                    <img src="--><?//=Url::home()?><!--image/sign-3.png" alt="">-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                        <div class="arr">
                            <p><img src="<?=Url::home()?>image/arr-b.png" alt=""></p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="small-section bg-gray-lighter">
                <div class="container-2 relative">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="small-item-carousel black owl-carousel animate-init owl-theme">
                                <?php foreach ($clients as $value):?>
                                <div class="owl-item" style="width: 175px;">
                                    <div class="logo-item">
                                        <h1>
                                            <a href="<?= $value['link']?>" target="_blank">
                                                <?= Html::img('/backend/web/upload/clients/thumbs/'.$value['img']);?>
                                            </a>
                                        </h1>
                                    </div>
                                </div>
                                <?php endforeach; ?>
<!--                                <div class="owl-item" style="width: 175px;">-->
<!--                                    <div class="logo-item">-->
<!--                                        <h1><img src="--><?//=Url::home()?><!--image/client_2.png" alt=""></h1></div>-->
<!--                                </div>-->
<!--                                <div class="owl-item" style="width: 175px;">-->
<!--                                    <div class="logo-item">-->
<!--                                        <h1><img src="--><?//=Url::home()?><!--image/client_3.png" alt=""></h1></div>-->
<!--                                </div>-->
<!--                                <div class="owl-item" style="width: 175px;">-->
<!--                                    <div class="logo-item">-->
<!--                                        <h1><img src="--><?//=Url::home()?><!--image/client_4.png" alt=""></h1></div>-->
<!--                                </div>-->
<!--                                <div class="owl-item" style="width: 175px;">-->
<!--                                    <div class="logo-item">-->
<!--                                        <h1><img src="--><?//=Url::home()?><!--image/client_5.png" alt=""></h1></div>-->
<!--                                </div>-->
<!--                                <div class="owl-item" style="width: 175px;">-->
<!--                                    <div class="logo-item">-->
<!--                                        <h1><img src="--><?//=Url::home()?><!--image/client_6.png" alt=""></h1></div>-->
<!--                                </div>-->
<!--                                <div class="owl-item" style="width: 175px;">-->
<!--                                    <div class="logo-item">-->
<!--                                        <h1><img src="--><?//=Url::home()?><!--image/client_1.png" alt=""></h1></div>-->
<!--                                </div>-->
<!--                                <div class="owl-item" style="width: 175px;">-->
<!--                                    <div class="logo-item">-->
<!--                                        <h1><img src="--><?//=Url::home()?><!--image/client_2.png" alt=""></h1></div>-->
<!--                                </div>-->
                            </div>
                        </div>  
                    </div>
                </div>
            </section>
        </section>
       
         
        <section id="work">
            <section class="page-section-np" id="portfolio">
                <div class="relative">
                    <div class="col-md-12">
                        <div class="container-2">
                            <div class="work-1">
                                <h6 class="cd-headline loading-bar">Awesome Portfolio.</h6>
                                <h1><?= $onepageoextdescription->awesome_portfolio_text ?></h1>
                                <br>
                                <br>
                                <div class="cher-line wow fadeInUp">
                                    <img src="<?=Url::home()?>image/cher-line.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container align-center">

                        <div class="mybutton-t small">
                            <a href="#" class="filter active" data-filter="*">
                                <span data-hover="ALL">ALL</span>
                            </a>
                        </div>
                        <?php foreach ($portfoliocats as $value):?>
                        <div class="mybutton-t small">
                            <a href="#" class="filter active" data-filter=".<?= $value->id?>">
                                <span data-hover="<?= $value->title?>"><?= $value->title?></span>
                            </a>
                        </div>
                        <?php endforeach; ?>

<!--                        <div class="mybutton-t small">-->
<!--                            <a href="#" class="filter active" data-filter=".brands">-->
<!--                                <span data-hover="Brands">Brands</span>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <div class="mybutton-t small">-->
<!--                            <a href="#" class="filter active" data-filter=".designing">-->
<!--                                <span data-hover="Designing">Designing</span>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <div class="mybutton-t small">-->
<!--                            <a href="#" class="filter active" data-filter=".fashion">-->
<!--                                <span data-hover="Fashion">Fashion</span>-->
<!--                            </a>-->
<!--                        </div>-->
                    </div>
                    <ul class="works-grid work-grid-3 clearfix" id="work-grid">

                        <?php foreach ($portfolio as $value):?>
                        <li class="work-item mix fashion design <?= $value['cat_id']?>">
                            <a href="portfolio/index/<?= $value->id ?>" class="work-ext-link">
                                <div class="work-img">
                                    <?= Html::img('/backend/web/upload/portfolio/thumbs/'.$value['img']);?>
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title"><?= $value['title']?></h3>
                                    <h2 class="work-descr"></h2>
                                </div>
                            </a>
                        </li>
                        <?php endforeach; ?>

<!--                        <li class="work-item mix fashion design">-->
<!--                            <a href="#" class="work-ext-link">-->
<!--                                <div class="work-img">-->
<!--                                    <img src="--><?//=Url::home()?><!--image/21.jpg" alt="">-->
<!--                                </div>-->
<!--                                <div class="work-intro">-->
<!--                                    <h3 class="work-title">Fashion</h3>-->
<!--                                    <h2 class="work-descr"></h2>-->
<!--                                </div>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="work-item mix fashion design">-->
<!--                            <a href="#" class="work-ext-link">-->
<!--                                <div class="work-img">-->
<!--                                    <img src="--><?//=Url::home()?><!--image/31.jpg" alt="Work">-->
<!--                                </div>-->
<!--                                <div class="work-intro">-->
<!--                                    <h3 class="work-title">Design &amp; Mode</h3>-->
<!--                                    <h2 class="work-descr"></h2>-->
<!--                                </div>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="work-item mix fashion design">-->
<!--                            <a href="#" class="work-ext-link">-->
<!--                                <div class="work-img">-->
<!--                                    <img src="--><?//=Url::home()?><!--image/41.jpg" alt="Work">-->
<!--                                </div>-->
<!--                                <div class="work-intro">-->
<!--                                    <h3 class="work-title">Branding</h3>-->
<!--                                    <h2 class="work-descr"></h2>-->
<!--                                </div>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="work-item mix fashion design">-->
<!--                            <a href="#" class="work-ext-link">-->
<!--                                <div class="work-img">-->
<!--                                    <img src="--><?//=Url::home()?><!--image/51.jpg" alt="Work">-->
<!--                                </div>-->
<!--                                <div class="work-intro">-->
<!--                                    <h3 class="work-title">Design &amp; Mode</h3>-->
<!--                                    <h2 class="work-descr"></h2>-->
<!--                                </div>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="work-item mix fashion design">-->
<!--                            <a href="#" class="work-ext-link">-->
<!--                                <div class="work-img">-->
<!--                                    <img src="--><?//=Url::home()?><!--image/7.jpg" alt="Work">-->
<!--                                <div class="work-intro">-->
<!--                                    <h3 class="work-title">Fashion</h3>-->
<!--                                    <h2 class="work-descr"></h2>-->
<!--                                </div>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="work-item mix fashion design">-->
<!--                            <a href="#" class="work-ext-link">-->
<!--                                <div class="work-img">-->
<!--                                    <img src="--><?//=Url::home()?><!--image/6.jpg" alt="Work">-->
<!--                                </div>-->
<!--                                <div class="work-intro">-->
<!--                                    <h3 class="work-title">Design &amp; Mode</h3>-->
<!--                                    <h2 class="work-descr"></h2>-->
<!--                                </div>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="work-item mix fashion design">-->
<!--                            <a href="#" class="work-ext-link">-->
<!--                                <div class="work-img">-->
<!--                                    <img src="--><?//=Url::home()?><!--image/9.jpg" alt="Work">-->
<!--                                </div>-->
<!--                                <div class="work-intro">-->
<!--                                    <h3 class="work-title">Design &amp; Mode</h3>-->
<!--                                    <h2 class="work-descr"></h2>-->
<!--                                </div>-->
<!--                            </a>-->
<!--                        </li>-->
                    </ul>
                </div>
            </section>

            <section class="page-section-dr">
                <div class="container-2">
                    <div class="col-md-8">
                        <div class="text-on-r">
                            <p><?= $onepageoextdescription->request_quote_text_1 ?></p>
                            <h1><?= $onepageoextdescription->request_quote_text_2 ?></h1>
                        </div>
                    </div>
                    <div class="col-md-4 text-on-r-w">
                        <div class="mybutton-w small mrg-tp-80">
                            <a href="contact/index">
                                <span data-hover="Request a Quote">Request a Quote</span>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
<!--            <div class="col-md-3 col-sm-6 bg-facts wow fadeInUp">-->
<!--                <div class="fact-sec">-->
<!--                    <i class="icon-wine"></i>-->
<!--                    <h1>Projects Done</h1>-->
<!--                    <div class="count-number">96</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-3 col-sm-6 bg-facts wow fadeInUp">-->
<!--                <div class="fact-sec">-->
<!--                    <i class="icon-bike"></i>-->
<!--                    <h1>Cycling KM</h1>-->
<!--                    <div class="count-number">1109</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-3 col-sm-6 bg-facts wow fadeInUp">-->
<!--                <div class="fact-sec">-->
<!--                    <i class="icon-gift"></i>-->
<!--                    <h1>Awards</h1>-->
<!--                    <div class="count-number">3</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-3 col-sm-6 bg-facts wow fadeInUp">-->
<!--                <div class="fact-sec">-->
<!--                    <i class="icon-pencil"></i>-->
<!--                    <h1>On Going</h1>-->
<!--                    <div class="count-number">7</div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <br>-->
        </section>
       
         
        <section id="contact">
            <section class="page-section-np" id="contacts">
                <div class="relative">
                    <div class="col-md-12">
                        <div class="container-2">
                            <div class="blog-1">
                                <h6 class="cd-headline loading-bar"><b>Contact </b>Us.</h6>
                                <h1><?= $onepageoextdescription->contact_us_text ?></h1>
                                <div class="cher-line" data-scroll-reveal="enter bottom move 10px over 2s after .5s" data-scroll-reveal-id="13" data-scroll-reveal-initialized="true">
                                    <img src="<?=Url::home()?>image/cher-line.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="small-section xwar-xat">
                <div class="container-2">
                    <div class="col-md-6">
                        <div class="form-container">
                            <div role="form" class="wpcf7" id="wpcf7-f170-o1" lang="en-US" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="#" method="post" class="wpcf7-form" novalidate="novalidate">
                                    <div>
                                        <input type="hidden" name="_wpcf7" value="170">
                                        <input type="hidden" name="_wpcf7_version" value="4.3">
                                        <input type="hidden" name="_wpcf7_locale" value="en_US">
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f170-o1">
                                        <input type="hidden" name="_wpnonce" value="f7c2625228">
                                    </div>
                                    <?php $form = ActiveForm::begin([
                                        'action' => ['site/contact'],
                                        'options' => [
                                            'enctype' => 'multipart/form-data',
//                                            'multiple' => true,
//                                            'class' => '',
                                        ]
                                    ]); ?>
                                    <div class="form contact-form" id="contact_form">
                                        <div class="form-group">
                                            <span class="wpcf7-form-control-wrap text-415">
                                                <?= $form->field($model, 'name')->textInput(['id' => '', 'class' => 'wpcf7-form-control wpcf7-text ci-field form-control', 'maxlength' => true,'placeholder' => "Name"])->label('') ?>
<!--                                                <input type="text" name="text-415" value="" size="40" class="wpcf7-form-control wpcf7-text ci-field form-control" id="Name" aria-invalid="false" placeholder="Name">-->
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <span class="wpcf7-form-control-wrap Email">
                                                <?= $form->field($model, 'email')->textInput(['id' => '', 'class' => 'wpcf7-form-control wpcf7-text wpcf7-url wpcf7-validates-as-required ci-field form-control', 'maxlength' => true,'placeholder' => "Email"])->label('') ?>
<!--                                                <input type="url" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-url wpcf7-validates-as-required wpcf7-validates-as-url ci-field form-control" id="Email" aria-required="true" aria-invalid="false" placeholder="Email">-->
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <span class="wpcf7-form-control-wrap Message">
                                                <?= $form->field($model, 'body')->textarea(['id' => '', 'class' => 'wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required ci-area form-control','rows'=>7,'maxlength' => true,'placeholder' => "Message"])->label('') ?>
<!--                                                <textarea name="Message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required ci-area form-control" id="Message" aria-required="true" aria-invalid="false" placeholder="Message"></textarea>-->
                                            </span>
                                            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                                            ]) ?>
                                        </div>
                                        <p>
                                            <?= Html::submitButton('Send Message', ['class' => 'wpcf7-form-control wpcf7-submit btn-cont']) ?>
<!--                                            <input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit btn-cont">-->
                                            <img class="ajax-loader" src="<?=Url::home()?>image/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;">
                                        </p>
                                        <div id="result"></div>
                                    </div>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                    <?php ActiveForm::end();?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="connect-1">
                            <h1>24 Azatutyan street, Yerevan, Armenia</h1>
                            <br>
                            <h2>_</h2>
                            <br>
                            <a href="mailto:hello@deepredmediasolutions.com"><h3>hello@deepredmediasolutions.com</h3></a>
                            <a href="tel:+374-11-900-901"><h4>Call Us: +374 11 900 901</h4></a>
                        </div>
                        <div class="connect-1">
                            <h1>Moscow Representative Office</h1>
                            <br>
                            <h2>_</h2>
                            <br>
                            <a href="mailto:moscow@deepredmediasolutions.com"><h3>moscow@deepredmediasolutions.com</h3></a>
                            <a href="tel:00-7926-4733631"><h4>Call Us: 00 (7926) 4733631</h4></a>
                        </div>
                        <div class="connect-1">
                            <h1>London Representative Office</h1>
                            <br>
                            <h2>_</h2>
                            <br>
                            <a href="mailto:london@deepredmediasolutions.com"><h3>london@deepredmediasolutions.com</h3></a>
                            <a href="tel:00-44-7714-059775"><h4>Call Us: 00 (44) 7714 059775</h4></a>
                        </div>
                    </div>
                </div>
            </section>
        </section>
