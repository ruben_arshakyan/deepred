<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Recipes | '.$model->title;

?>

        <div class="top_panel_title top_panel_style_1  title_present breadcrumbs_present scheme_original">
            <div class="top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner">
                <div class="content_wrap">
                    <h1 class="page_title"><?= $model->title ?></h1>
                </div>
            </div>
        </div>

        <div class="page_content_wrap single single-post postid-1044 single-format-standard origano_body body_style_wide body_filled theme_skin_origano article_style_stretch layout_single-standard template_single-standard top_panel_show top_panel_above sidebar_show sidebar_right sidebar_outer_hide mmm mega_main_menu-2-0-7 wpb-js-composer js-comp-ver-4.7.4 vc_responsive top_panel_fixed">

            <div class="content_wrap">
                <div class="content">
                    <article class="itemscope post_item post_item_single post_featured_default post_format_standard post-1044 post type-post status-publish format-standard has-post-thumbnail hentry category-masonry category-portfolio tag-tag2 tag-tag3 tag-tag1 tag-portfolio tag-video" itemscope="" itemtype="http://schema.org/Review">
                        <section class="post_featured">
                            <div class="post_thumb recipe-info" data-image="<?= Url::to('/backend/web/upload/recipe/thumbs/'.$model->img);?>" data-title="Organic Farming Feeds the World">
                                <?php if(!empty($model->img)){ ?>
                                <a id="recipe-thumb" class="hover_icon hover_icon_view inited tumbnail-recipe fl" href="<?= Url::to('/backend/web/upload/recipe/'.$model->img);?>">
                                    <?= Html::img('/backend/web/upload/recipe/thumbs/'.$model->img);?>
                                </a>
                                <?php }else{
                                    echo Html::img('/frontend/web/image/images.png');
                                } ?>
                                <div class="ingredient-block">
                                    <h3><?= Yii::t('file','ingredients');?></h3>
                                    <?= \frontend\controllers\RecipeController::getProducts($model->parrent_id,\backend\models\Lang::getCurrent()->url) ?>
                                </div>
                                <?php $gallery = \common\models\Func::getGallery("recipe/".$model->parrent_id.'/thumbs'); ?>
                                <!--                                --><?php //\common\models\Func::d($gallery); ?>
                                <?php if($gallery){ ?>
                                    <!--                                --><?php //\common\models\Func::d("a") ?>
                                    <div class="small-gallery-recipe" id="thumbnails">
                                        <?php foreach($gallery as $one){ ?>
                                            <a class="inline" href="<?= Url::to('/backend/web/upload/recipe/'.$model->parrent_id.'/'.$one)?>">
                                                <img class="img" src="<?= Url::to('/backend/web/upload/recipe/'.$model->parrent_id.'/thumbs/'.$one) ?>" data-url = "<?= Url::to('/backend/web/upload/ingredients/'.$model->parrent_id.'/'.$one)?>"/>
                                            </a>
                                        <?php }?>
                                    </div>
                                <?php }?>
                            </div>
                        </section>


                        <section class="post_content" itemprop="reviewBody">
                            <h1 itemprop="name" class="product_title entry-title"><?= Yii::t('file','recipe-description');?></h1>
                            <p><?= $model->description ?></p>
                        </section>
                        <!-- </section> class="post_content" itemprop="reviewBody"> -->

                    </article>
                    <!-- </article> class="itemscope post_item post_item_single post_featured_default post_format_standard post-1044 post type-post status-publish format-standard has-post-thumbnail hentry category-masonry category-portfolio tag-tag2 tag-tag3 tag-tag1 tag-portfolio tag-video" itemscope itemtype="http://schema.org/Review"> -->

                </div>
                <!-- </div> class="content"> -->

                <?php $this->beginContent('@app/views/layouts/sidebar-recipe.php'); ?>


                <?php $this->endContent(); ?>

            </div>
            <!-- </div> class="content_wrap"> -->
        </div>
        <!-- </.page_content_wrap> -->

