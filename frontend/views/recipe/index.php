<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Func;
use backend\models\Lang;

$this->title = 'Recipes';

?>
        <div class="top_panel_title top_panel_style_1  title_present breadcrumbs_present scheme_original">
            <div class="top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner">
                <div class="content_wrap">
                    <h1 class="page_title"><?= Yii::t('file','menu-recipes');?></h1>
                </div>
            </div>
        </div>

        <div class="page_content_wrap archive category category-masonry category-6 origano_body body_style_wide body_filled theme_skin_origano article_style_stretch layout_masonry_2 template_masonry top_panel_show top_panel_above sidebar_show sidebar_right sidebar_outer_hide mmm mega_main_menu-2-0-7 wpb-js-composer js-comp-ver-4.7.4 vc_responsive top_panel_fixed">

            <div class="content_wrap">
                <div class="content">
                    <div class="isotope_wrap inited" data-columns="2" style="position: relative;">
                        <?php foreach($models as $model): ?>
                        <div class="isotope_item isotope_item_masonry isotope_item_masonry_2 isotope_column_2 isotope_item_show fl" style="opacity: 1; ">
                            <article class="post_item post_item_masonry post_item_masonry_2 post_format_standard odd">

                                <h5 class="post_title"><a href="<?= Url::to(['recipe/view', 'id' => $model->parrent_id, 's' => $model->slug]) ?>"><?= $model->title ?></a></h5>
                                <div class="sc_line sc_line_style_solid" style="border-top-style:solid;"></div>

                                <div class="post_featured">
                                    <a class="hover_icon hover_icon_link" href="<?= Url::to(['recipe/view', 'id' => $model->parrent_id, 's' => $model->slug]) ?>">
                                        <?php if(!empty($model->img)) {?>
                                        <div class="recipe-post_thumb" data-image="<?= Url::to('/backend/web/upload/recipe/thumbs/'.$model->img);?>" style="background-image: url( <?php echo Url::to('/backend/web/upload/recipe/thumbs/'.$model->img);?>);" data-title="">

                                        </div>
                                        <?php }else{ ?>
                                        <div class="recipe-post_thumb" data-image="<?= Url::to('/frontend/web/image/images.png');?>" style="background-image: url( <?php echo Url::to('/backend/web/upload/recipe/thumbs/'.$model->img);?>);" data-title="">

                                        </div>
                                        <?php } ?>
                                    </a>
                                </div>

                                <div class="post_content isotope_item_content">

                                    <div class="post_descr">
                                        <p><?= Func::getExcerpt($model->description,0,100) ?></p><a href="<?= Url::to(['recipe/view', 'id' => $model->parrent_id, 's' => $model->slug]) ?>" class="post_readmore sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_medium"><span class="post_readmore_label"><?= Yii::t('file','view');?></span></a>
                                    </div>
                                </div>
                                <!-- /.post_content -->
                            </article>
                            <!-- /.post_item -->
                        </div>
                        <?php endforeach; ?>
                        <?php
                        echo LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>
                    </div>
                    <!-- /.isotope_wrap -->

                </div>
                <!-- </div> class="content"> -->
                <?php $this->beginContent('@app/views/layouts/sidebar-recipe.php'); ?>


                <?php $this->endContent(); ?>

            </div>
            <!-- </div> class="content_wrap"> -->
        </div>
        <!-- </.page_content_wrap> -->