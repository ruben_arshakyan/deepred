<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Newss | '.$model->title;
?>

        <div class="top_panel_title top_panel_style_1  title_present breadcrumbs_present scheme_original">
            <div class="top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner">
                <div class="content_wrap">
                    <h1 class="page_title"><?= $model->title ?></h1>
                </div>
            </div>
        </div>

        <div class="page_content_wrap">

            <div class="content_wrap single single-post postid-421 single-format-standard origano_body body_style_wide body_filled theme_skin_origano article_style_stretch layout_single-standard template_single-standard top_panel_show top_panel_above sidebar_show sidebar_right sidebar_outer_hide mmm mega_main_menu-2-0-7 wpb-js-composer js-comp-ver-4.7.4 vc_responsive top_panel_fixed">
                <div class="content">
                    <article class="itemscope post_item post_item_single post_featured_default post_format_standard post-421 post type-post status-publish format-standard has-post-thumbnail hentry category-healthy-diet category-masonry category-portfolio tag-masonry tag-tag1" itemscope="" itemtype="">
                        <section class="post_featured">
                            <div class="post_thumb" data-image="<?= Url::to(['/backend/web/upload/posts/thumbs/'.$model->img]) ?>" data-title="Define Eating Plan">
                                <a class="hover_icon hover_icon_view inited" href="<?= Url::to(['/backend/web/upload/posts/thumbs/'.$model->img]) ?>" title="Define Eating Plan" rel="magnific">
                                    <?= Html::img('/backend/web/upload/posts/thumbs/'.$model->img, ['width' => 1170, 'height' => 660]);?>
                                </a>
                            </div>
                        </section>

                        <section class="post_content" itemprop="articleBody">
                            <p><?= $model->description ?></p>
                        </section>
                        <!-- </section> class="post_content" itemprop="articleBody"> -->
                    </article>

                </div>
                <!-- </div> class="content"> -->


            </div>
            <!-- </div> class="content_wrap"> -->
        </div>
        <!-- </.page_content_wrap> -->