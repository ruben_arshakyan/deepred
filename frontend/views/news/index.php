<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\models\Func;

$this->title = 'News';
?>
        <div class="top_panel_title top_panel_style_1  title_present breadcrumbs_present scheme_original">
            <div class="top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner">
                <div class="content_wrap">
                    <h1 class="page_title"><?= Yii::t('file','all-news');?></h1>
                </div>
            </div>
        </div>

        <div class="page_content_wrap page page-id-305 page-template page-template-blog page-template-blog-php origano_body body_style_wide body_filled theme_skin_origano article_style_stretch layout_excerpt template_excerpt top_panel_show top_panel_above sidebar_show sidebar_right sidebar_outer_hide mmm mega_main_menu-2-0-7 wpb-js-composer js-comp-ver-4.7.4 vc_responsive top_panel_fixed">

            <div class="content_wrap">
                <div class="content">
                    <?php foreach($model as $news): ?>
                    <article class="post_item post_item_excerpt post_featured_default post_format_standard odd post-1044 post type-post status-publish format-standard has-post-thumbnail hentry category-masonry category-portfolio tag-tag2 tag-tag3 tag-tag1 tag-portfolio tag-video">
                        <h3 class="post_title"><a href="<?= Url::to(['news/view', 'id' => $news->parrent_id, 's' => $news->slug]) ?>"><span style="display:none;" class="post_icon icon-book-open"></span><?= $news->title ?></a></h3>
                        <div class="sc_line sc_line_style_solid" style="border-top-style:solid;"></div>
                        <div class="post_featured">
                            <div class="post_thumb" data-image="<?= Url::to(['/backend/web/upload/posts/thumbs/'.$news->img]) ?>" data-title="Organic Farming Feeds the World">
                                <a class="hover_icon hover_icon_link" href="<?= Url::to(['news/view', 'id' => $news->parrent_id, 's' => $news->slug]) ?>">
                                    <?= Html::img('/backend/web/upload/posts/'.$news->img, ['width' => 870, 'height' => 490]);?>
                                </a>
                            </div>
                        </div>

                        <div class="post_content clearfix">

                            <div class="post_descr">
                                <p><?= Func::getExcerpt($news->description,0,300) ?></p>
                                <a href="<?= Url::to(['news/view', 'id' => $news->parrent_id, 's' => $news->slug]) ?>" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_medium"><?= Yii::t('file','about-view');?></a> </div>

                        </div>
                        <!-- /.post_content -->

                    </article>
                    <!-- /.post_item -->

                    <?php endforeach; ?>

                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>

                </div>


            </div>
            <!-- </div> class="content_wrap"> -->
        </div>
        <!-- </.page_content_wrap> -->