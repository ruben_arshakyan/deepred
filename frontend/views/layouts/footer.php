<?php
use yii\helpers\Url;
?>

<script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>
            <a href="#" data-anim-delay="0" id="see-map">
                <div class="title-on-m">
                    <img src="<?=Url::home()?>image/on-m.png" alt="">
                </div>
            </a>
            <div id="google-map" style="width: 100%; height: 550px; display: none !important;"></div>

            <footer class="footer small-section">
            <div class="container-2">

                <div class="col-md-6 col-sm-6">
                    <div class="footer-social-links">
                        <div class="social-links tooltip-bot" data-original-title="" title="">
							<a href="https://www.facebook.com/drmcommunity" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/drmcommunity" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
					        <a href="https://www.linkedin.com/company/deepred-media-solutions" title="" data-original-title="Dribbble"><i class="fa fa-linkedin"></i></a>
							<a href="https://plus.google.com/109926809183914259815" title="" data-original-title="google +"><i class="fa fa-google-plus"></i></a>
<!--							<a href="#" title="" data-original-title="Flickr"><i class="fa fa-flickr"></i></a>			-->
                        </div>
                    </div>
                </div>



                <div class="col-md-5 col-sm-5">
                    <div class="footer-text">
						<div class="footer-copy">
                            <a href="http://www.deepredmediasolutions.com/"> <span></span></a>.
                        </div>
                        <div class="footer-made">
                            <span>DeepRedMediaSolutions</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-1 col-sm-1 ft-log">
                    <div class="footer-text animate-init" data-anim-type="fade-in" data-anim-delay="1000">
                        <a href="#"><img src="<?=Url::home()?>image/logo-3.png"></a>
                    </div>
                </div>
            </div>
            </footer>
