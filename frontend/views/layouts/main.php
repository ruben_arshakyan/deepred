<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use common\models\Statistics;

/* @var $this \yii\web\View */
/* @var $content string */
$isEnabled = \backend\models\Settings::isEnabled();

if($isEnabled == 1){
    $st = new Statistics();
    $st->setStatistics();
}

AppAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="DeepRedMediaSolutions">

    <link rel='stylesheet' id='googlefont-css'  href='http://fonts.googleapis.com/css?family=Lato%3A300%2C400%2C700%2C900&#038;ver=4.3.3' type='text/css' media='all'>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="home page page-id-8 page-template page-template-frontpage page-template-frontpage-php">

<div class="appear-animate body-push">

    <div class="page-loader" style="display: none;">
        <div id="loader"></div>
        <img src="<?=Url::home()?>image/logo-p.png" alt="">
    </div>

    <div class="page" id="top">
        <!-- End Page Loader -->

        <!-- BACKGROUND -->

        <!--<img src="<?=Url::home()?>image/1.jpg" style="background-image: url("<?=Url::home()?>")image/1.jpg" >-->

    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php

        if ((Yii::$app->controller->id == 'site') && (Yii::$app->controller->action->id == 'index'))
        {
            $this->beginContent('@app/views/layouts/header-onepage.php');
            $this->endContent();
        }else{
            $this->beginContent('@app/views/layouts/header.php');
            $this->endContent();
        }
        ?>


        <div class="wrap">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>
    </div>
    </div>

        <?php $this->beginContent('@app/views/layouts/footer.php');
              $this->endContent();
        ?>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
