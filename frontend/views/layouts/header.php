<?php
use yii\helpers\Url;
?>

        <div class="menu-right" id="theMenu">
            <div class="menu-wrap">
                <h1 class="logo">
                    <a href="<?=Url::home()?>">DeepRedMediaSolutions</a>
                </h1>

                <nav>

                    <ul class="menu-items">
                        <li id="menu-item-176" class="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/site/index"><span>Home</span></a>
                        </li>
                        <li id="menu-item-10" class="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/careers/index"><span>Careers</span></a>
                        </li>
                        <li id="menu-item-96" class="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/services/index"><span>Services</span></a>
                        </li>
                        <li id="menu-item-315" class="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/blog/index"><span>Blog</span></a>
                        </li>
                        <li id="menu-item-115" class="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/portfolio/portfolio"><span>Portfolio</span></a>
                        </li>
                        <li id="menu-item-166" class="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/contact/index"><span>Contact</span></a>
                        </li>
                    </ul>
                </nav>
            </div>

            <div id="toggle-right" class="toggle-light">
                <img src="<?=Url::home()?>image/nav-bt.png" alt="">
            </div>

        </div>

        <div class="nav-bar-compact clearfix js-nbc-bg">

            <div class="nbc-logo-wrap local-scroll">
                <a href="<?=Url::home()?>" class="nbc-logox">
                    <h1 class="logo-name">
                        <img src="<?=Url::home()?>image/logo.png" alt="">
                    </h1>
                </a>
            </div>
        </div>