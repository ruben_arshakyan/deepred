<div class="top_panel_title top_panel_style_1  title_present breadcrumbs_present scheme_original">
    <div class="top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner">
        <div class="content_wrap">
            <h1 class="page_title"><?= Yii::t('file','vacancy'); ?></h1>

        </div>
    </div>
</div>

<div class="page_content_wrap page page-id-639 page-template-default origano_body body_style_wide body_filled theme_skin_origano article_style_stretch layout_single-standard template_single-standard top_panel_show top_panel_above sidebar_show sidebar_right sidebar_outer_hide mmm mega_main_menu-2-0-7 wpb-js-composer js-comp-ver-4.7.4 vc_responsive">

    <div class="content_wrap">
        <div class="content">
            <article class="itemscope post_item post_item_single post_featured_default post_format_standard post-639 page type-page status-publish hentry" itemscope="" itemtype="http://schema.org/Article">
                <section class="post_content" itemprop="articleBody">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="wpb_wrapper">
                                <?php foreach($model as $one): ?>
                                <h1 class="sc_title sc_title_regular" style="margin-top:0px;"><?= $one->title ?></h1>
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p><?= $one->description ?></p>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </section>
        </div>


