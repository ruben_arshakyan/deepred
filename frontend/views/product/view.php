<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\Lang;


$this->title = 'Products | '.$model->title;
//\common\models\Func::d($model);
?>


    <div class="top_panel_title top_panel_style_1  title_present breadcrumbs_present scheme_original">
        <div class="top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner">
            <div class="content_wrap">
                <h1 class="page_title"><?= $model->title ?></h1>
            </div>
        </div>
    </div>

    <div class="page_content_wrap single single-product postid-610 origano_body body_style_wide body_filled theme_skin_origano article_style_stretch layout_single-standard template_single-standard top_panel_show top_panel_above sidebar_show sidebar_right sidebar_outer_hide mmm mega_main_menu-2-0-7 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.7.4 vc_responsive">

        <div class="content_wrap">
            <div class="content">
                <article class="post_item post_item_single post_item_product" id="product-review">

                    <div itemscope="" itemtype="" id="product-610" class="post-610 product type-product status-publish has-post-thumbnail product_cat-vegetables product_tag-fresh product_tag-veggies shipping-taxable purchasable product-type-simple product-cat-vegetables product-tag-fresh product-tag-veggies instock">

                        <div class="images">
                            <?php if(!empty($model->img)){ ?>
                            <a id="product-thumb" href="<?= Url::to('/backend/web/upload/ingredients/'.$model->img)?>" class="woocommerce-main-image hover_icon hover_icon_view">
                                <?= Html::img('/backend/web/upload/ingredients/thumbs/'.$model->img, ['width' => 600, 'height' => 600]);?>
                            </a>
                            <?php }else{
                                echo Html::img('/frontend/web/image/images.png', ['width' => 600, 'height' => 600]);
                            } ?>
                            <?php $gallery = \common\models\Func::getGallery("ingredients/".$model->parrent_id.'/thumbs');
//                                \common\models\Func::d($gallery);
                            ?>

                            <?php if($gallery){ ?>
                                <div class="small-gallery" id="thumbnails">
                                    <a class="inline" href="<?= Url::to('/backend/web/upload/ingredients/'.$model->img)?>">
                                        <img class="img" src="<?= Url::to('/backend/web/upload/ingredients/thumbs/'.$model->img) ?>" data-url = "<?= Url::to('/backend/web/upload/ingredients/'.$model->img)?>"/>
                                    </a>
                                    <?php foreach($gallery as $one){ ?>
                                        <a class="inline" href="<?= Url::to('/backend/web/upload/ingredients/'.$model->parrent_id.'/'.$one)?>">
                                            <img class="img" src="<?= Url::to('/backend/web/upload/ingredients/'.$model->parrent_id.'/thumbs/'.$one) ?>" data-url = "<?= Url::to('/backend/web/upload/ingredients/'.$model->parrent_id.'/'.$one)?>"/>
                                        </a>
                                    <?php }?>
                                </div>
                            <?php }?>
                        </div>

                        <div class="summary entry-summary">

                            <h1 itemprop="name" class="product_title entry-title"><?= $model->title ?></h1>

                            <div itemprop="description" class="short-description">
                                <?php if(!empty($model->country_of_origin)){ ?>
                                <p><span><?= Yii::t('file','country');?>: </span><?= $model->country_of_origin ?></p>
                                <?php } ?>
                                <?php if(!empty($model->usage_area)){ ?>
                                <p><span><?= Yii::t('file','usage');?>: </span><?= $model->usage_area ?></p>
                                <?php } ?>
                                <?php if(!empty($model->taste_specification)){ ?>
                                <p><span><?= Yii::t('file','taste');?>: </span><?= $model->taste_specification ?></p>
                                <?php } ?>
                                <?php if(!empty($model->available_weight)){ ?>
                                <p><span><?= Yii::t('file','weight');?>: </span><?= $model->available_weight ?></p>
                                <?php } ?>
                            </div>

                        </div>
                        <!-- .summary -->

                        <div class="woocommerce-tabs">

                            <div class="panel entry-content" id="tab-description" style="display: block;">

                                <h2><?= Yii::t('file','product-description');?></h2>

                                <?= $model->description ?>
                                <?= $model->video ?>
                            </div>

                    </div>
                    <!-- #product-610 -->

                </article>
                <!-- .post_item -->

            </div>
            <!-- </div> class="content"> -->

            <?php $this->beginContent('@app/views/layouts/sidebar-product.php'); ?>


            <?php $this->endContent(); ?>

        </div>
        <!-- </div> class="content_wrap"> -->
    </div>
    <!-- </.page_content_wrap> -->