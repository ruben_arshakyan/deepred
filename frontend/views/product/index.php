<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Func;
use yii\web\Linkable;
use yii\widgets\LinkPager;

//\common\models\Func::d($model);


$this->title = $categoryName != null ? $categoryName : Yii::t('file','menu-product');

?>


    <div class="top_panel_ sdfgsfg title top_panel_style_1  title_present breadcrumbs_present scheme_original">
        <div class="top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner">
            <div class="content_wrap">
                <h1 class="page_title"><?= $this->title ?></h1>
            </div>
        </div>
    </div>

    <div class="page_content_wrap archive post-type-archive post-type-archive-product origano_body body_style_wide body_filled theme_skin_origano article_style_stretch layout_excerpt template_excerpt top_panel_show top_panel_above sidebar_show sidebar_right sidebar_outer_hide mmm mega_main_menu-2-0-7 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.7.4 vc_responsive">

        <div class="content_wrap">
            <div class="content">
                <div class="list_products shop_mode_thumbs">

                    <ul class="products product-index " id="product-list">

                        <?php foreach($model as $products): ?>
                        <li class="col-lg-4 first post-610 product type-product status-publish has-post-thumbnail product_cat-vegetables product_tag-fresh product_tag-veggies  column-1_3 shipping-taxable purchasable product-type-simple product-cat-vegetables product-tag-fresh product-tag-veggies instock">

                            <a href="#">

                            </a>
                            <div class="post_item_wrap">
                                <a href="<?= Url::to(['product/view', 'id' => $products->parrent_id, 's' => $products->slug]) ?>">

                                </a>
                                <div class="post_featured">
                                    <a href="<?= Url::to(['product/view', 'id' => $products->parrent_id, 's' => $products->slug]) ?>">
                                    </a>
                                    <div class="post_thumb">
                                        <a href="<?= Url::to(['product/view', 'id' => $products->parrent_id, 's' => $products->slug]) ?>">
                                        </a>
                                        <a class="hover_icon hover_icon_link" href="<?= Url::to(['product/view', 'id' => $products->parrent_id, 's' => $products->slug]) ?>">
                                            <?php if(!empty($products->img)) {
                                                echo Html::img('/backend/web/upload/ingredients/thumbs/'.$products->img);
                                            }else{
                                                echo Html::img('/frontend/web/image/images.png');
                                            }
                                            ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="post_content">

                                    <h3><a href="#"><?= $products->title ?></a></h3>

                                    <div class="description">
                                        <p><?= Func::getExcerpt($products->description,0,100) ?></p>
                                    </div>

                                    <a href="<?= Url::to(['product/view', 'id' => $products->parrent_id, 's' => $products->slug]) ?>" rel="nofollow" data-product_id="610" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple"><?= Yii::t('file','view');?></a> </div>
                            </div>

                        </li>

                        <?php endforeach; ?>

                    </ul>
                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
                <!-- .list_products -->

            </div>
            <!-- </div> class="content"> -->
            <?php $this->beginContent('@app/views/layouts/sidebar-product.php'); ?>


            <?php $this->endContent(); ?>

        </div>
        <!-- </div> class="content_wrap"> -->
    </div>
    <!-- </.page_content_wrap> -->
