<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Func;

$this->title = 'Portfolio';
?>

<section id="work">
    <section class="page-section-np" id="portfolio">
        <div class="relative">
            <div class="col-md-12">
                <div class="container-2">
                    <div class="work-1">
                        <h6 class="cd-headline loading-bar">Awesome Portfolio.</h6>
                        <h1><?= $onepageoextdescription->awesome_portfolio_text ?></h1>
                        <br>
                        <br>
                        <div class="cher-line wow fadeInUp">
                            <img src="<?=Url::home()?>image/cher-line.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container align-center">

                <div class="mybutton-t small">
                    <a href="#" class="filter active" data-filter="*">
                        <span data-hover="ALL">ALL</span>
                    </a>
                </div>
                <?php foreach ($portfoliocats as $value):?>
                    <div class="mybutton-t small">
                        <a href="#" class="filter active" data-filter=".<?= $value->id?>">
                            <span data-hover="<?= $value->title?>"><?= $value->title?></span>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <ul class="works-grid work-grid-3 clearfix" id="work-grid">

                <?php foreach ($portfolio as $value):?>
                    <li class="work-item mix fashion design <?= $value['cat_id']?>">
                        <a href="/portfolio/index/<?= $value->id ?>" class="work-ext-link">
                            <div class="work-img">
                                <?= Html::img('/backend/web/upload/portfolio/thumbs/'.$value['img']);?>
                            </div>
                            <div class="work-intro">
                                <h3 class="work-title"><?= $value['title']?></h3>
                                <h2 class="work-descr"></h2>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>

    <section class="page-section-dr">
        <div class="container-2">
            <div class="col-md-8">
                <div class="text-on-r">
                    <p><?= $onepageoextdescription->request_quote_text_1 ?></p>
                    <h1><?= $onepageoextdescription->request_quote_text_2 ?></h1>
                </div>
            </div>
            <div class="col-md-4 text-on-r-w">
                <div class="mybutton-w small mrg-tp-80">
                    <a href="/contact/index">
                        <span data-hover="Request a Quote">Request a Quote</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
</section>
